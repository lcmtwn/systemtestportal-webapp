/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package email

import "testing"

func TestNew(t *testing.T) {
	to := []string{"TestTo"}
	bcc := []string{"TestBcc"}
	cc := []string{"TestCC"}
	subject := "TestSubject"
	text := []byte("TestText")
	html := []byte("TestHTML")

	email := New(to, bcc, cc, subject, text, html)

	for i, emailTo := range email.To {
		if emailTo != to[i] {
			t.Errorf("the to-attribute of the email was not set correctly, was %v, expected %v", emailTo, to[i])
		}
	}
	for i, emailBcc := range email.Bcc {
		if emailBcc != bcc[i] {
			t.Errorf("the bcc-attribute of the email was not set correctly, was %v, expected %v", emailBcc, bcc[i])
		}
	}
	for i, emailCc := range email.Cc {
		if emailCc != cc[i] {
			t.Errorf("the cc-attribute of the email was not set correctly, was %v, expected %v", emailCc, cc[i])
		}
	}
	if email.Subject != subject {
		t.Errorf("the subject of the email was not set correctly, was %v, expected %v", email.Subject, subject)
	}
	for i, emailText := range email.Text {
		if emailText != text[i] {
			t.Errorf("the text of the email was not set correctly, was %v, expected %v", emailText, text)
			break
		}
	}
	for i, emailHTML := range email.HTML {
		if emailHTML != html[i] {
			t.Errorf("the html of the email was not set correctly, was %v, expected %v", emailHTML, html)
			break
		}
	}
}
