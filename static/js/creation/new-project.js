/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/
$.getScript("/static/js/util/common.js");

/**
 * Adds the listeners on page load (executed in new-project.tmpl)
 */
function initializeProjectCreateListener() {

// attach a handler to the import input
    $("#importProject").change(function () {
        validateImport(event);
    });

    $("#inputProjectLogo").change(function () {
        validateProjectImage(this);
    });
}

/**
 * Validate the import for file-type json
 */
function validateImport() {

    let file = document.getElementById('importProject').files[0];

    // Check if file has type json
    if (file.type !== "application/json") {
        document.getElementById('importProject').value = "";
        displayWrongFileModal();
    }
}

/**
 * Validate the input for file-type image
 */
function validateProjectImage(input) {

    function applyImage(image) {
        var reader = new FileReader();

        reader.onload = e => $('#previewProjectLogo')
            .attr('src', e.target.result);

        reader.readAsDataURL(image);
    }

    if (input.files && input.files[0]) {
        let file = input.files[0];
        if (file.type.startsWith("image")) {
            applyImage(file);

        } else {
            document.getElementById('inputProjectLogo').value = "";
            displayWrongFileModal();
        }
    }
}

/**
 * Display Modal for worng file
 */
function displayWrongFileModal() {
    $('#errorModalTitle').html("File not supported");
    $('#errorModalBody').html("This file ist not supported. Please select a valid file.");
    $('#modal-generic-error').modal('show');
}

/* attach a handler to the create button */
$("#newProjectForm").submit(function (event) {

    let uploadedFile = document.getElementById('importProject').files[0];

    /* stop form from submitting normally */
    event.preventDefault();

    /* get the action attribute from the <form action=""> element */
    const url = $(this).attr('action');

    var jsonString = "";

    if (uploadedFile != null) {

        // reader for import
        let reader = new FileReader();
        reader.onload = function (e) {

            let contents = e.target.result;
            var json = JSON.parse(contents);
            jsonString = JSON.stringify(json);

            $.post(url, {

                inputProjectName: $('#inputProjectName').val(),
                inputProjectDesc: $('#inputProjectDesc').val(),
                inputProjectLogo: $('#previewProjectLogo').attr("src"),
                optionsProjectVisibility: document.querySelector('input[name="optionsProjectVisibility"]:checked').value,
                inputImport: jsonString

            }).done((request, textStatus, data) => {

                window.location.href = urlify("").appendCodedSegment(data.getResponseHeader("signedInUser")).appendCodedSegment(data.getResponseHeader('newName')).toString();

            }).fail(request => {

                $("#modalPlaceholder").empty().append(request.responseText);
                $('#errorModal').modal('show')

            });
        };
        reader.readAsText(uploadedFile);


    } else {

        /* Send the data using post with element ids*/
        $.post(url, {

            inputProjectName: $('#inputProjectName').val(),
            inputProjectDesc: $('#inputProjectDesc').val(),
            inputProjectLogo: $('#previewProjectLogo').attr("src"),
            optionsProjectVisibility: document.querySelector('input[name="optionsProjectVisibility"]:checked').value,

        }).done((request, textStatus, data) => {

            window.location.href = urlify("").appendCodedSegment(data.getResponseHeader("signedInUser")).appendCodedSegment(data.getResponseHeader('newName')).toString();

        }).fail(request => {

            $("#modalPlaceholder").empty().append(request.responseText);
            $('#errorModal').modal('show')

        });
    }
});

/**
 * Keyboard support
 */
$(document)
    .off("keydown.event")
    .on("keydown.event", function (event) {
        keyboardSupport(event)
    });


/**
 * keybindings
 * @param event
 */
function keyboardSupport(event) {
    //checks if focus is in textfields, areas, etc.
    switch (event.target.tagName) {
        case "INPUT":
        case "SELECT":
        case "TEXTAREA":
            return;
    }

    //Adds Keybindings
    let buttonString;
    switch (event.key) {
        case "Enter":
            buttonString = "buttonCreateProject";
            break;
    }
    if (document.getElementById(buttonString)) {
        document.getElementById(buttonString);
    }
}

/**
 * Adds key listeners when modals closes
 */
$('.modal').on('hide.bs.modal', function (event) {
    $(document).on("keydown.event", function (event) {
        keyboardSupport(event);
    });
});

/**
 * Removes key listeners when modals open
 */
$('.modal').on('show.bs.modal', function (e) {
    $(document).off("keydown.event");

});
