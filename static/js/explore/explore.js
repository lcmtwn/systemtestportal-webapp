/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Will load and display the DataTable.
 * @param idTable {string} - Unique id of the table (id="...")
 * @param idSearch {string}  - Unique id of the search input (id="...")
 * @param listElementsPerPage {number} - Amount of entries per page until pagination comes into play
 * @param listElements {string} - Which elements are shown in the table (projects, groups or users)
 */
function initializeDataTable(idTable, idSearch, listElementsPerPage, listElements) {

    const table = $(`#${idTable}`);
    const dataTable = table.DataTable({
        "sDom": '<"top">rt<"bottom"p><"clear">',

        "pageLength": listElementsPerPage,
        drawCallback: function() {
            const pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        },

        "language": {
            "zeroRecords":     `No ${listElements} found matching query ...`
        }
    });

    $(`#${idSearch}`).on("keyup", function () {
        dataTable.search( this.value ).draw()
    });

    table.animate({ opacity: 1 }, 150)
}