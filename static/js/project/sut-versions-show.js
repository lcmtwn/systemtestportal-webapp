/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/


// testCaseVariantData contains the versions and variants of the test case
var testObjectVariantData = {};

// fills Variants into the dropdown
function fillVersions(versions, selectorID) {
    $(selectorID).empty();
    $.each(versions, function (key, version) {
        addVersion(version, selectorID);
    });
}

// adds a version entry to the dropdown
function addVersion(version, selectorID) {
    var selector = $(selectorID);
    var selectedValue = selector.val();
    selector.append($("<option></option>")
        .attr("value", version.Name)
        .text(version.Name));
    selector.value = selectedValue;
}


// Update the list with variants when selecting another version
function setVersionOnChangeListener(versionSelectorID, variantSelectorID) {
    $(versionSelectorID).on('change', function () {
        updateVariantShowList(versionSelectorID, variantSelectorID)
    });
}

// Update the dropdown with variants when selecting another version
function setVersionDropdownOnChangeListener(versionMap, versionSelectorID, variantSelectorID) {
    $(versionSelectorID).on('change', function () {
        updateVariantDropdown(versionMap, versionSelectorID, variantSelectorID);
    });
}

// Updates the list with the variants
function updateVariantShowList(versionSelectorID, variantSelectorID) {
    var versionKey = $(versionSelectorID).val();
    if (typeof versionKey === 'undefined'){
        return;
    }
    var list = $(variantSelectorID);
    // remove all previously shown elements
    list.empty();
    // add versions of selected version to list
    $.each(testObjectVersionData[versionKey].Variants, function (key, variant) {
        var listElement = ($("<li></li>")
            .attr("class", "list-group-item")
            .html('<span>' + variant.Name + '</span>'));
        list.append(listElement)
    });
}

// Updates the dropdown with the variants
function updateVariantDropdown(data, versionSelectorID, variantSelectorID) {
    var versionKey = $(versionSelectorID).val();
    var dropdown = $(variantSelectorID);
    // remove all previously shown elements
    dropdown.empty();
    // add versions of selected variant to list
    var variants;
    if(data != null && data[versionKey] && data[versionKey].Variants) {
        variants = data[versionKey].Variants;
    }

    $.each(variants, function (key, variant) {
        dropdown.append($("<option></option>")
            .attr("value", variant.Name)
            .text(variant.Name));
    });
}

