/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/common.js");
$.getScript("/static/js/util/ajax.js");

// Adds the listeners on page load (exectuted in content-group-tabs.tmpl)
function initializeGroupTabClickListener() {
    //$("#tabButtonActivity").click(function(event) {requestTab(event,"activity", false, 2)});
    //$("#tabButtonProjects").click(function(event) {requestTab(event,"projects", false, 2)});
    //$("#tabButtonMembers").click(function(event) {requestTab(event,"members", false, 2)});
    //$("#tabButtonSettings").click(function(event) {requestTab(event,"settings", false, 2)});
}

// highlights the active tab on page load
$(document).ready(function () {
    var url = currentURL();
    // noinspection FallThroughInSwitchStatementJS
    switch (url.segments[2]) {
        case "activity":
            $("#tabButtonActivity").addClass("active");
        case "projects":
            // Not available disabled
        case "members":
            // Not available disabled
    }
});