/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

// The different static parts a url can be made of
const (
	Register       = "/register"
	SignIn         = "/signin"
	SignOut        = "/signout"
	SystemSettings = "/settings"

	About = "/about"

	Explore = "/explore"

	Dashboard = "/dashboard"
	Tasks     = "/tasks"

	Permission = "/permission"

	Projects = "/projects"
	Users    = "/users"
	Groups   = "/groups"

	Cases     = "/testcases"
	Sequences = "/testsequences"
	Protocols = "/protocols"
	Members   = "/members"
	Comment   = "/comment"
)

// The actions that can be taken on an item
const (
	Async             = "/async"
	Show              = "/"
	New               = "/new"
	Edit              = "/edit"
	Update            = "/update"
	Duplicate         = "/duplicate"
	Delete            = "/delete"
	History           = "/history"
	Execute           = "/execute"
	Upload            = "/upload"
	Save              = "/save"
	Print             = "/print"
	PrintPdf          = "/pdf"
	PrintMd           = "/md"
	JSON              = "/json"
	Info              = "/info"
	Versions          = "/versions"
	Settings          = "/settings"
	Labels            = "/labels"
	Roles             = "/roles"
	Add               = "/add"
	Remove            = "/remove"
	Tester            = "/tester"
	SequenceDashboard = "/sequences"
	Export            = "/export"
	Markdown          = "/markdown"
	Sanitize          = "/sanitize"
	Closed            = "/closed"
	Opened            = "/opened"
)

// The variables a url part can be
const (
	VarContainer = "/:container"
	VarProject   = "/:project"
	VarSequence  = "/:testsequence"
	VarCase      = "/:testcase"
	VarProfile   = "/:profile"
)

const (
	SettingsNav = "/nav"
	SettingsTab = "/tab"

	SettingsProject    = "/settings/project"
	SettingsPermission = "/settings/permission"
)
