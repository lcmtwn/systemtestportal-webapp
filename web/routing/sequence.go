/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/assignment"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/deletion"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/duplication"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/execution"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/json"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/list"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/printing"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/update"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func registerSequencesHandler(pg *contextGroup, n *negroni.Negroni, tss store.SequencesSQL) {

	tsStore := store.GetSequenceStore()
	tcStore := store.GetCaseStore()

	tssg := wrapContextGroup(pg.NewContextGroup(Sequences))

	tssg.HandlerGet(Show,
		n.With(negroni.WrapFunc(list.SequencesGet(tsStore))))
	tssg.HandlerGet(New,
		n.With(negroni.WrapFunc(display.CreateSequenceGet(tcStore))))
	tssg.HandlerGet(Save,
		n.With(negroni.Wrap(defaultRedirect("../"))))
	tssg.HandlerGet(JSON,
		n.With(negroni.WrapFunc(json.SequencesGet(tsStore))))
	tssg.HandlerGet(Info,
		n.With(negroni.WrapFunc(json.SequenceInfoGet(tcStore))))
	tssg.HandlerGet(Print,
		n.With(negroni.WrapFunc(printing.SequencesListGet(tsStore))))

	tssg.HandlerPost(Save,
		n.With(negroni.WrapFunc(creation.SequencePost(tcStore, tsStore, tsStore))))

	registerSequenceHandler(tssg, n, tss)
}

func registerSequenceHandler(tssg *contextGroup, n *negroni.Negroni, testsequenceStore store.SequencesSQL) {
	us := store.GetUserStore()

	tsStore := store.GetSequenceStore()
	tcStore := store.GetCaseStore()
	commentStore := store.GetCommentStore()
	taskStore := store.GetTaskStore()

	tsg := wrapContextGroup(tssg.NewContextGroup(VarSequence))

	tsg.HandlerGet(Show, n.With(middleware.TestSequence(tsStore), negroni.WrapFunc(display.ShowSequenceGet(commentStore, taskStore))))
	tsg.HandlerPut(Show, n.With(middleware.TestSequence(tsStore), negroni.WrapFunc(creation.SequenceCommentPut(commentStore))))
	tsg.HandlerGet(Edit,
		n.With(middleware.TestSequence(testsequenceStore),
			negroni.WrapFunc(display.EditSequenceGet(tcStore))))
	tsg.HandlerGet(History,
		n.With(middleware.TestSequence(testsequenceStore),
			negroni.WrapFunc(display.HistorySequenceGet)))
	tsg.HandlerGet(JSON,
		n.With(middleware.TestSequence(testsequenceStore),
			negroni.WrapFunc(json.SequenceGet)))
	tsg.HandlerGet(Print,
		n.With(middleware.TestSequence(testsequenceStore),
			negroni.WrapFunc(printing.SequenceGet)))

	tsg.HandlerPost(Duplicate,
		n.With(middleware.TestSequence(testsequenceStore), negroni.WrapFunc(duplication.SequencePost(tsStore, tsStore))))

	tsg.HandlerPut(Update,
		n.With(middleware.TestSequence(testsequenceStore),
			negroni.WrapFunc(update.SequencePut(tcStore, tsStore, tsStore, commentStore, taskStore))))
	tsg.HandlerPut(Tester,
		n.With(middleware.TestSequence(testsequenceStore),
			negroni.WrapFunc(assignment.SequencePut(us, taskStore, taskStore, taskStore, taskStore))))

	tsg.HandlerDelete("",
		n.With(middleware.TestSequence(testsequenceStore),
			negroni.WrapFunc(deletion.SequenceDelete(testsequenceStore))))

	registerSequenceExecuteHandler(tsg, n, testsequenceStore, tcStore, tcStore, taskStore, taskStore)
	registerSequenceLabelsHandler(tsg, n, testsequenceStore)
}

func registerSequenceExecuteHandler(tsg *contextGroup, n *negroni.Negroni,
	testSequenceStore middleware.TestSequenceStore, tcs middleware.TestCaseStore, caseLister handler.TestCaseLister,
	taskAdder handler.TaskListAdder, taskGetter handler.TaskListGetter) {

	protocols := store.GetProtocolStore()

	tsg.HandlerGet(Execute,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(execution.SequenceStartPageGet(protocols))))
	tsg.HandlerPost(Execute,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(execution.SequenceExecutionPost(protocols, protocols, protocols, tcs, caseLister, taskAdder, taskGetter))))
	tsg.HandlerPut(Execute,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(execution.SequenceExecutionPut(protocols, protocols, tcs, protocols, taskAdder, taskGetter))))
}

func registerSequenceLabelsHandler(tsg *contextGroup, n *negroni.Negroni,
	testsequenceStore store.SequencesSQL) {

	tsg.HandlerGet(Labels,
		n.With(middleware.TestSequence(testsequenceStore),
			negroni.WrapFunc(json.SequenceLabelsGet)))
	tsg.HandlerPost(Labels,
		n.With(middleware.TestSequence(testsequenceStore),
			negroni.WrapFunc(json.SequenceLabelsPost(testsequenceStore))))
}
