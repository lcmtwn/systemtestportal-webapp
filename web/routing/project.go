/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"net/http"

	"github.com/dimfeld/httptreemux"
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/ci_integration"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/deletion"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/export"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/json"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/update"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"gitlab.com/stp-team/systemtestportal-webapp/web/sessions"
)

func registerProjectsHandler(authRouter *httptreemux.ContextMux) {
	userStore := store.GetUserStore()
	groupStore := store.GetGroupStore()
	projectStore := store.GetProjectStore()
	commentStore := store.GetCommentStore()

	tcStore := store.GetCaseStore()
	tsStore := store.GetSequenceStore()
	protocolStore := store.GetProtocolStore()

	n := negroni.New(middleware.Container(userStore, groupStore), middleware.Project(projectStore))

	sessionStore := sessions.GetSessionStore()
	sessionNegroni := negroni.New(middleware.Auth(sessionStore))
	registerProjectCreateHandler(authRouter, sessionNegroni, projectStore, tcStore, tsStore)

	pg := wrapContextGroup(authRouter.NewContextGroup(VarContainer + VarProject))

	registerProjectHandler(pg, n, projectStore)
	registerCasesHandler(pg, n, tcStore)
	registerSequencesHandler(pg, n, tsStore)
	registerProtocolsHandler(pg, n, tcStore, tsStore, protocolStore)
	registerMembersHandler(pg, n)
	registerDashboardHandler(pg, n, tcStore, tsStore, protocolStore, protocolStore)
	registerProjectExportHandler(pg, n, tcStore, tsStore)
	registerCommentsHandler(pg, n, commentStore)
	registerCIIntegrationHandler(pg, n, tcStore, protocolStore, projectStore)
}

func registerProjectCreateHandler(authRouter *httptreemux.ContextMux, nBasic *negroni.Negroni, projectStore store.ProjectsSQL, tcStore store.CasesSQL, tsStore store.SequencesSQL) {

	authRouter.Handler(http.MethodGet, Projects+New,
		nBasic.With(negroni.WrapFunc(display.CreateProjectGet)))
	authRouter.Handler(http.MethodPost, Projects+Save,
		nBasic.With(negroni.WrapFunc(creation.ProjectPost(projectStore, projectStore, tcStore, tcStore, tcStore, tsStore))))
}

func registerCIIntegrationHandler(pg *contextGroup, n *negroni.Negroni, caseStore store.CasesSQL,
	protocolStore store.ProtocolsSQL, projectStore store.ProjectsSQL) {

	taskStore := store.GetTaskStore()
	pg.HandlerPost("/ci",
		n.With(negroni.WrapFunc(
			ci_integration.IntegrationPost(caseStore, protocolStore, taskStore, taskStore, projectStore))))
}

func registerProjectHandler(pg *contextGroup, n *negroni.Negroni, ps store.ProjectsSQL) {

	pg.HandlerGet(Show,
		n.With(negroni.Wrap(defaultRedirect("dashboard"))))

	registerProjectVersionsHandler(pg, n, ps)
	registerProjectSettingsHandler(pg, n, ps)
	registerProjectLabelsHandler(pg, n, ps)
	registerProjectRolesHandler(pg, n, ps)
}

func registerProjectVersionsHandler(pg *contextGroup, n *negroni.Negroni, ps handler.ProjectAdder) {
	pg.HandlerGet(Versions,
		n.With(negroni.WrapFunc(json.ProjectVariantsGet)))
	pg.HandlerPost(Versions,
		n.With(negroni.WrapFunc(json.ProjectVariantsPost(ps))))
}

func registerProjectSettingsHandler(cg *contextGroup, n *negroni.Negroni, ps store.ProjectsSQL) {

	cg.HandlerGet(SettingsProject,
		n.With(negroni.WrapFunc(display.GetSettingsProjects)))
	cg.HandlerGet(SettingsProject+Async+SettingsNav,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsProjectsNav)))
	cg.HandlerGet(SettingsProject+Async+SettingsTab,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsProjectsTab)))
	cg.HandlerPost(SettingsProject,
		n.With(negroni.WrapFunc(update.ProjectPost(ps, ps))))
	cg.HandlerDelete(SettingsProject,
		n.With(negroni.WrapFunc(deletion.ProjectDelete(ps))))

	cg.HandlerGet(SettingsPermission,
		n.With(negroni.WrapFunc(display.GetSettingsPermissions)))
	cg.HandlerGet(SettingsPermission+Async+SettingsNav,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsPermissionsNav)))
	cg.HandlerGet(SettingsPermission+Async+SettingsTab,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsPermissionsTab)))
	cg.HandlerPut(SettingsPermission+Roles,
		n.With(negroni.WrapFunc(update.ProjectRolesPut(ps))))
	cg.HandlerDelete(SettingsPermission+Roles+Delete,
		n.With(negroni.WrapFunc(deletion.ProjectRoleDelete(ps))))
}

func registerProjectLabelsHandler(pg *contextGroup, n *negroni.Negroni, ps handler.ProjectLabelUpdater) {
	pg.HandlerGet(Labels,
		n.With(negroni.WrapFunc(json.ProjectLabelsGet)))
	pg.HandlerPost(Labels,
		n.With(negroni.WrapFunc(json.ProjectLabelsPost(ps))))
	pg.HandlerPut(Labels,
		n.With(negroni.WrapFunc(update.ProjectLabelsPut(ps))))
}

func registerProjectRolesHandler(pg *contextGroup, n *negroni.Negroni, ps handler.ProjectAdder) {
	pg.HandlerGet(Roles,
		n.With(negroni.WrapFunc(json.ProjectRolesGet)))
	//pg.HandlerPut(Roles,
	//	n.With(negroni.WrapFunc(update.ProjectRolesPut(ps))))
}

func registerProjectExportHandler(pg *contextGroup, n *negroni.Negroni, caseStore store.CasesSQL, sequenceStore store.SequencesSQL) {
	pg.HandlerPost(SettingsProject+Export, n.With(negroni.WrapFunc(export.ProjectJson(caseStore, sequenceStore))))
}

func registerCommentsHandler(pg *contextGroup, n *negroni.Negroni, commentStore handler.Comments) {
	pg.HandlerPut(Comment, n.With(negroni.WrapFunc(update.CommentPut(commentStore))))
}
