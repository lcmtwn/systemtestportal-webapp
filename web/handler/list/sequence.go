/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"encoding/json"
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/color"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// SequencesGet simply serves the page that lists sequences.
func SequencesGet(lister handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tss, err := lister.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		filter := r.FormValue(httputil.Filter)
		tss, filters, err := filterSequences(filter, tss, c.Project)

		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tmpl := getTestSequenceListFragment(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.Filtered, filters).
			With(context.TestSequences, tss).
			With(context.LabelColors, color.GetLabelColors()).
			With(context.DeleteLabels, modal.LabelDeleteMessage), tmpl, w, r)
	}
}

func filterSequences(filter string, sequences []*test.Sequence, proj *project.Project) ([]*test.Sequence, []project.Label, error) {
	var filters []project.Label

	// return immediately if there are no filters
	if filter == "[]" || filter == "" || !IsJSON(filter) {
		return sequences, filters, nil
	}

	// get applied filters from JSON string
	var values []string
	bytes := []byte(filter)

	err := json.Unmarshal(bytes, &values)

	if err != nil {

		return sequences, filters, err
	}

	// turn string array into project.Label array
	for _, f := range values {
		label, _ := proj.GetLabelByName(f)
		filters = append(filters, project.Label{
			Name:        f,
			Description: label.Description,
			Color:       label.Color,
		})
	}

	i := 0
	for _, c := range sequences {
		if containsLabels(filters, &c.Labels) {
			sequences[i] = c
			i++
		}
	}
	sequences = sequences[:i]

	return sequences, filters, nil
}

func getTestSequenceListFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesListFragment(r)
	}
	return getTabTestSequencesListTree(r)
}

// getTabTestSequencesListTree returns the test sequence list tab template with all parent templates
func getTabTestSequencesListTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence list tree
		Append(templates.TestSequencesList, templates.ManageLabels, templates.DeletePopUp).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesListFragment returns only the test sequence list tab template
func getTabTestSequencesListFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.TestSequencesList, templates.ManageLabels, templates.DeletePopUp).
		Get().Lookup(templates.TabContent)
}
