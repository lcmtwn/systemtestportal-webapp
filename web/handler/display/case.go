/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// ShowCaseGet simply serves the page that displays a test case.
func ShowCaseGet(commentStore handler.Comments, assigneeGetter handler.TaskGetter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		comments, err := commentStore.GetCommentsForTest(*c.Case, c.Project, c.User)

		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tasks, err := assigneeGetter.GetTasksForTest(c.Case.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		ShowCase(c.Project, c.Case, comments, tasks, w, r)
	}
}

// ShowCase tries to respond with the display page for a case.
// If an error occurs an error response is sent instead.
func ShowCase(p *project.Project, tc *test.Case, c []*comment.Comment, tasks []*task.Item, w http.ResponseWriter, r *http.Request) {
	tmpl := getTabTestCaseShow(r)
	tcv, err := handler.GetTestCaseVersion(r, tc)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	ctx := context.New().
		WithUserInformation(r).
		With(context.Project, p).
		With(context.TestCase, tc).
		With(context.TestCaseVersion, tcv).
		With(context.Tasks, tasks).
		With(context.DurationHours, int(tcv.Duration.Hours())).
		With(context.DurationMin, tcv.Duration.GetMinuteInHour()).
		With(context.DurationSec, tcv.Duration.GetSecondInMinute()).
		With(context.Comments, c).
		With(context.DeleteTestCase, modal.TestCaseDeleteMessage)

	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getTabTestCaseShow returns either the test case show fragment only or the show fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabTestCaseShow(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestCasesShowFragment(r)
	}
	return getTabTestCasesShowTree(r)
}

// getTabTestCasesShowTree returns the test case show tab template with all parent templates
func getTabTestCasesShowTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test case show tree
		Append(templates.ShowTestCase).
		Append(templates.Comments).
		Append(templates.Comment).
		Append(templates.DeletePopUp).
		Append("modal/tester-assignment").
		Get().Lookup(templates.HeaderDef)
}

// getTabTestCasesShowFragment returns only the test case show tab template
func getTabTestCasesShowFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.ShowTestCase).
		Append(templates.Comments).
		Append(templates.Comment).
		Append(templates.DeletePopUp).
		Append("modal/tester-assignment").
		Get().Lookup(templates.TabContent)
}

// CreateCaseGet simply serves the site that is used to create new testcases.
func CreateCaseGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).CreateCase {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	tmpl := getTestCaseNewFragment(r)
	handler.PrintTmpl(context.New().
		WithUserInformation(r).
		With(context.Project, c.Project), tmpl, w, r)
}

// getTestCaseNewFragment returns either only the test case new fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getTestCaseNewFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestCasesNewFragment(r)
	}
	return getTabTestCasesNewTree(r)
}

// getTabTestCasesNewTree returns the new test case tab template with all parent templates
func getTabTestCasesNewTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs, templates.TestStepEdit, templates.ManageVersions, templates.DeletePopUp).
		// Tab test case new tree
		Append(templates.NewTestCase).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestCasesNewFragment returns only the new test case tab template
func getTabTestCasesNewFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.NewTestCase, templates.TestStepEdit, templates.ManageVersions, templates.DeletePopUp).
		Get().Lookup(templates.TabContent)
}

// EditCaseGet simply serves the edit page for editing testcases.
func EditCaseGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.Case == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).EditCase {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	tmpl := getTabTestCaseEdit(r)
	tcv, err := handler.GetLatestTestCaseVersion(r, c.Case)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	ctx := context.New().
		WithUserInformation(r).
		With(context.Project, c.Project).
		With(context.TestCase, c.Case).
		With(context.TestCaseVersion, tcv).
		With(context.DurationHours, int(tcv.Duration.Hours())).
		With(context.DurationMin, tcv.Duration.GetMinuteInHour()).
		With(context.DurationSec, tcv.Duration.GetSecondInMinute())
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getTabTestCaseEdit returns either the test case edit fragment only or the edit fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabTestCaseEdit(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestCasesEditFragment(r)
	}
	return getTabTestCasesEditTree(r)
}

// getTabTestCasesEditTree returns the test case edit tab template with all parent templates
func getTabTestCasesEditTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs, templates.TestStepEdit, templates.ManageVersions).
		// Tab test case edit tree
		Append(templates.EditTestCase).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestCasesEditFragment returns only the test case edit template
func getTabTestCasesEditFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.EditTestCase, templates.TestStepEdit, templates.ManageVersions).
		Get().Lookup(templates.TabContent)
}

// HistoryCaseGet just serves the history page showing the
// history of a case.
func HistoryCaseGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.Case == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	tmpl := getTabTestCaseHistory(r)
	ctx := context.New().
		WithUserInformation(r).
		With(context.Project, c.Project).
		With(context.TestCase, c.Case)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getTabTestCaseHistory returns either only the test case history
// fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getTabTestCaseHistory(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestCasesHistoryFragment(r)
	}
	return getTabTestCasesHistoryTree(r)

}

// getTabTestCasesHistoryTree returns the test case history tab template with all parent templates
func getTabTestCasesHistoryTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test case history tree
		Append(templates.TestCaseHistory).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestCasesHistoryFragment returns only the test case history template
func getTabTestCasesHistoryFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.TestCaseHistory).
		Get().Lookup(templates.TabContent)
}
