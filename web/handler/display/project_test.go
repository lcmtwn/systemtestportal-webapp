/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCreateProjectGet(t *testing.T) {

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)

	tested := CreateProjectGet
	handler.Suite(t,
		handler.CreateTest("No user in context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal project",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestProjectSettingsGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:   nil,
			middleware.ContainerKey: nil,
			middleware.UserKey:      nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:   handler.DummyProject,
			middleware.ContainerKey: handler.DummyUser,
			middleware.UserKey:      handler.DummyUser,
		},
	)
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:   handler.DummyProject,
			middleware.ContainerKey: handler.DummyUser,
			middleware.UserKey:      nil,
		},
	)
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:      handler.DummyUserUnauthorized,
			middleware.ProjectKey:   handler.DummyProject,
			middleware.ContainerKey: handler.DummyUser,
		},
	)

	tested := GetSettingsProjects
	handler.Suite(t,
		handler.CreateTest("Empty context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No user in context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleRequest(ctxNoUser, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
			handler.SimpleRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
