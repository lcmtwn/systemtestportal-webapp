/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Package ci_integration provides an integration into the CI/CD to analyze test results.

The protocols of the test cases for a system-under-test version, which
is given as parameter by the POST-Request, will be analyzed.

The response can either be "Passed" or "Failed"
The tests in the stp are classified as follows:
Not executed tests: Failed
Executed tests can have different results:
	- Passed: Passed
	- Passed with comment: Passed
	- Failed: Failed
	- Not assessed: Failed
*/
package ci_integration
