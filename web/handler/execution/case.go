/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/store/storeuploads"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// CaseProtocolStore interface for storing testcase protocols.
type CaseProtocolStore interface {
	CaseProtocolAdder
	CaseProtocolGetter
	// GetCaseExecutionProtocols gets the protocols for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocols(testCaseID id.TestID) ([]test.CaseExecutionProtocol, error)
	GetCaseExecutionProtocolsForProject(projectID id.ProjectID) ([]test.CaseExecutionProtocol, error)
}

// CaseProtocolAdder is used to add new testcase protocols to the store.
type CaseProtocolAdder interface {
	// AddCaseProtocol adds the given protocol to the store
	AddCaseProtocol(r *test.CaseExecutionProtocol, testCaseVersion test.CaseVersion) (err error)
}

// CaseProtocolGetter is used to get testcase protocols from the store.
type CaseProtocolGetter interface {
	// GetCaseExecutionProtocol gets the protocol with the given id for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error)
}

// CaseStartPageGet serves the start page for testcase executions.
func CaseStartPageGet() http.HandlerFunc {
	printer := caseExecutionPrinter{}
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).Execute {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tcv, err := handler.GetTestCaseVersion(r, c.Case)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		printer.printStartPage(w, r, *c.Case, *tcv, 1, len(tcv.Steps)+3, nil, false)
	}
}

// CaseExecutionPost handles all post requests during a testcase execution. It is a meta handler
// that further calls it's sub handlers.
func CaseExecutionPost(protocolLister test.ProtocolLister, caseProtocolStore CaseProtocolStore,
	// Params for sequence summary page
	sequenceProtocolAdder SequenceProtocolAdder,
	progress progressMeter, testSequenceVersion *test.SequenceVersion, testCaseGetter handler.TestCaseGetter,
	testCaseLister handler.TestCaseLister, listAdder handler.TaskListAdder, listGetter handler.TaskListGetter) http.HandlerFunc {

	p := caseExecutionPrinter{}
	sp := sequenceExecutionPrinter{
		caseProtocolStore,
	}

	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.User == nil || c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).Execute {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tcv, err := handler.GetTestCaseVersion(r, c.Case)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		previous := getFormValueBool(r, keyIsPrevious)

		// The passed progress is nil if this is the execution of a case and it needs to be
		// initialized as caseProgress. If this is the execution of a sequence, the passed
		// progress is a sequenceProgress.
		if progress == nil {
			progress = &caseProgress{}
		}
		if err := progress.Init(r, previous); err != nil {
			errors.Handle(err, w, r)
			return
		}

		currentStepNr := getFormValueInt(r, keyStepNr)
		totalStepsNr := len(tcv.Steps)
		if !previous {
			switch {
			case totalStepsNr == 0:
				handleCaseSummaryPage(w, r, protocolLister, caseProtocolStore,
					sequenceProtocolAdder, p, sp, progress, testSequenceVersion, testCaseGetter, testCaseLister, listAdder, listGetter)
			case currentStepNr == 0:
				handleCaseStartPage(w, r, &p, tcv, progress)
			case currentStepNr > 0 && currentStepNr < totalStepsNr+1:
				handleCaseStepPage(w, r, &p, tcv, progress, caseProtocolStore, previous)
			case currentStepNr == totalStepsNr+1:
				handleCaseSummaryPage(w, r, protocolLister, caseProtocolStore,
					sequenceProtocolAdder, p, sp, progress, testSequenceVersion, testCaseGetter,
					testCaseLister, listAdder, listGetter)
			default:
				executionPageNotFound(w, r)
			}
		} else {
			switch {
			case currentStepNr == 0:
				if inSequence(r) {
					if caseNr := getFormValueInt(r, keyCaseNr); caseNr > 1 {
						tc := testSequenceVersion.Cases[caseNr-2]
						tcv := tc.TestCaseVersions[0]
						timerTime, err := getCurrentDuration(r)
						if err != nil {
							errors.Handle(err, w, r)
							return
						}
						p.printSummaryPage(w, r, tcv, progress.Get()-1, progress.Max(), timerTime, true)

					}
				}
			case currentStepNr == 1:
				if inSequence(r) {
					seqPr, _ := GetCurrentSequenceProtocol(r)
					p.printStartPage(w, r, *c.Case, *tcv, progress.Get()-1, progress.Max(), seqPr, previous)
				} else {
					p.printStartPage(w, r, *c.Case, *tcv, progress.Get()-1, progress.Max(), nil, previous)

				}
				currentStepNr = 0
			case currentStepNr > 1 && currentStepNr <= totalStepsNr+1:
				handleCaseStepPage(w, r, &p, tcv, progress, caseProtocolStore, true)
			default:
				executionPageNotFound(w, r)
			}
		}
	}
}

func handleCaseStartPage(w http.ResponseWriter, r *http.Request,
	caseExecutionPrinter *caseExecutionPrinter, testCaseVersion *test.CaseVersion, progress progressMeter) {

	time, err := getCurrentDuration(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	caseExecutionPrinter.printStepPage(w, r, *testCaseVersion, 1, progress.Get(), progress.Max(), time, false)
}

func handleCaseStepPage(w http.ResponseWriter, r *http.Request,
	caseExecutionPrinter *caseExecutionPrinter, testCaseVersion *test.CaseVersion,
	progress progressMeter, protocolStore CaseProtocolStore, previous bool) {

	stepNr := getFormValueInt(r, keyStepNr)

	time, err := getCurrentDuration(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	if !previous {
		// Case is finished -> show summary page
		if stepNr == len(testCaseVersion.Steps) {
			caseExecutionPrinter.printSummaryPage(w, r, *testCaseVersion, progress.Get(), progress.Max(), time, false)
			return
		}

		caseExecutionPrinter.printStepPage(w, r, *testCaseVersion, stepNr+1, progress.Get(), progress.Max(), time, previous)
	} else {
		caseExecutionPrinter.printStepPage(w, r, *testCaseVersion, stepNr-1, progress.Get(), progress.Max(), time, previous)
	}

}

func handleCaseSummaryPage(w http.ResponseWriter, r *http.Request, protocolLister test.ProtocolLister,
	caseProtocolAdder CaseProtocolAdder,
	// Params for the sequence summary page
	sequenceProtocolAdder SequenceProtocolAdder,
	caseExecutionPrinter caseExecutionPrinter, sequenceExecutionPrinter sequenceExecutionPrinter,
	progress progressMeter, testSequenceVersion *test.SequenceVersion, testCaseGetter handler.TestCaseGetter,
	// Params for the ci integration
	testCaseLister handler.TestCaseLister,
	taskAdder handler.TaskListAdder, taskGetter handler.TaskListGetter) {

	prot, err := getCurrentCaseProtocol(r, protocolLister)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	tcv, err := getCaseVersionForProtocol(prot, testCaseGetter)

	if !inSequence(r) {

		err = caseProtocolAdder.AddCaseProtocol(&prot, tcv)

		if err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				failedSave, unableToSaveSummaryPage, r).
				WithLog("Error while trying to save protocol into store.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}
	}

	// end of sequence execution
	if inSequence(r) {
		if seqPr, _ := GetCurrentSequenceProtocol(r); seqPr != nil {
			//seqPr.CaseExecutionProtocols = append(seqPr.CaseExecutionProtocols, id.NewProtocolID(prot.TestVersion, prot.ProtocolNr))

			handleSequenceSummaryPage(w, r,
				caseExecutionPrinter, sequenceExecutionPrinter, progress, testSequenceVersion,
				seqPr, protocolLister, seqPr, taskAdder, taskGetter)
		}
	}

	// end of case execution
	if !inSequence(r) {
		// close the related tasks
		err := setTaskToDone(r, tcv.ID().TestID, taskAdder, taskGetter, prot.SUTVersion, prot.SUTVariant)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		// If the case execution finished,
		// redirect back to case if everything worked fine
		url := strings.TrimSuffix(r.URL.Path, "?fragment=true")
		url = strings.TrimSuffix(url, "/execute")
		url = url + "?fragment=true"

		http.Redirect(w, r, url, http.StatusSeeOther)
	}
}

// getCurrentCaseProtocol unmarshals the protocol from the request form and adds required meta data to it
func getCurrentCaseProtocol(r *http.Request, protocolLister test.ProtocolLister) (test.CaseExecutionProtocol, error) {
	prot := test.CaseExecutionProtocol{}
	protJson := r.FormValue(keyCaseProtocol)
	err := json.Unmarshal([]byte(protJson), &prot)
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}

	protocolNr, err := test.GetProtocolNr(protocolLister, prot.TestVersion)
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}

	prot.ProtocolNr = protocolNr
	prot.ExecutionDate = time.Now()

	return prot, nil
}

// getCaseVersionForProtocol gets the correct CaseVersion for the protocol
func getCaseVersionForProtocol(prot test.CaseExecutionProtocol, testCaseGetter handler.TestCaseGetter) (test.CaseVersion, error) {
	tc, ex, err := testCaseGetter.Get(prot.TestVersion.TestID)
	if err != nil {
		return test.CaseVersion{}, err
	}
	if !ex {
		return test.CaseVersion{}, err

	}
	tcv := tc.TestCaseVersions[len(tc.TestCaseVersions)-prot.TestVersion.TestVersion()]

	return tcv, nil
}

// getCurrentDuration gets the duration that is stored in the request form
func getCurrentDuration(r *http.Request) (duration.Duration, error) {
	time := duration.Duration{}

	err := json.Unmarshal([]byte(r.FormValue(keyDuration)), &time)
	if err != nil {
		return duration.Duration{}, err
	}

	return time, nil
}

// inSequence returns the isInSequence key value from the request form
func inSequence(r *http.Request) bool {
	return r.FormValue(keyIsInSeq) == "true"
}

func Upload() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)

		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		upload := r.FormValue(httputil.UploadSource)
		uploadName := r.FormValue(httputil.UploadName)

		imagePath, err := storeuploads.WriteUploadToFile(uploadName, upload, *c.Project)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		response, err := json.Marshal("/" + imagePath)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		_, err = w.Write(response)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

// setTaskToDone sets the related tasks of the user to done
func setTaskToDone(r *http.Request, test id.TestID, listAdder handler.TaskListAdder, listGetter handler.TaskListGetter, version string, variant string) error {
	c := handler.GetContextEntities(r)
	if c.User == nil {
		return c.Err
	}

	list, err := listGetter.Get(c.User.Name)
	if err != nil {
		return err
	}
	var refType task.ReferenceType
	if test.IsCase() {
		refType = task.Case
	} else {
		refType = task.Sequence
	}
	list = list.SetTasksToDone(task.Assignment, refType, test.Test(), version, variant)

	return listAdder.Add(*list)
}
