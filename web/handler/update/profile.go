/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package update

import (
	"net/http"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"strconv"
)

func EditProfilePut(ul handler.UserLister, ua handler.UserAdder, uv handler.UserValidator) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser := handler.GetContextEntities(r).User

		users, err := ul.List()
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		requestedUserName := r.FormValue("user")
		password := r.FormValue("userPassword")
		var targetUser *user.User = nil
		for _, user := range users {
			if user.Name == requestedUserName {
				targetUser = user
			}
		}


		vUser, valid, err := uv.Validate(requestedUserName, password)
		if currentUser != nil && targetUser != nil && err == nil  {
			if targetUser.ID() == currentUser.ID() && valid && vUser != nil{
				invalidFields := []string{}

				email := r.FormValue("email")
				bio := r.FormValue("bio")

				newPassword := r.FormValue("newPassword")
				if newPassword != "" {
					password = newPassword
				}

				isShownPublic, err := strconv.ParseBool(r.FormValue("isShownPublic"))
				if err != nil {
					invalidFields = append(invalidFields, "IsShownPublic is not a boolean")
				}

				isEmailPublic, err := strconv.ParseBool(r.FormValue("isEmailPublic"))
				if err != nil {
					invalidFields = append(invalidFields, "IsEmailPublic is not a boolean")
				}

				targetUser.Email = email
				targetUser.Biography = bio
				targetUser.IsShownPublic = isShownPublic
				targetUser.IsEmailPublic = isEmailPublic
				pwUserRel := user.PasswordUser{
					Password: password,
					User:     *targetUser,
				}
				err = ua.Add(&pwUserRel)
				if err == nil && len(invalidFields) == 0 {
					w.WriteHeader(200)
				} else {
					var responseText string

					responseText = responseText + "Invalid Fields:"
					for i := 0; i < len(invalidFields); i++ {
						responseText = responseText + "\n" + " - " + invalidFields[i]
					}
					responseText = responseText + "\n\n"

					w.WriteHeader(400)
					w.Write([]byte(responseText))
				}

			} else {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			}
		} else {
			if currentUser == nil {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			} else {
				errors.Handle(errors.ConstructStd(http.StatusNotFound, "Profile not found", "The user you requested does not exist", r).Finish(), w, r)
				return
			}
		}
	}
}