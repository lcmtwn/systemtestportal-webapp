/*
This file is part of SystemTestPortal.
Copyright (C) 2018  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"testing"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"net/http"
)

func TestProjectLabelsPut(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
			middleware.UserKey:    handler.DummyUser,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    handler.DummyUser,
		},
	)
	body := "[]"
	normalBody := "[{\"Name\":\"Test\",\"Description\":\"A test label\",\"Color\":\"ff0000\"},{\"Name\":\"Test2\",\"Description\":\"\",\"Color\":\"7c4596\"}]"
	handler.Suite(t,
		handler.CreateTest("Empty Context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectLabelUpdaterMock{}
				return ProjectLabelsPut(a), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(&a.ProjectAdderMock, 0),
					handler.HasCalls(&a.ProjectLabelRenamerMock, 0),
				)
			},
			handler.NewRequest(invalidCtx, http.MethodPut, handler.NoParams, body),
			handler.NewFragmentRequest(invalidCtx, http.MethodPut, handler.NoParams, body),
		),
		handler.CreateTest("Invalid body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectLabelUpdaterMock{}
				return ProjectLabelsPut(a), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(&a.ProjectAdderMock, 0),
					handler.HasCalls(&a.ProjectLabelRenamerMock, 0),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, ""),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, ""),
		),
		handler.CreateTest("No Label",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectLabelUpdaterMock{}
				return ProjectLabelsPut(a), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(&a.ProjectAdderMock, 0),
					handler.HasCalls(&a.ProjectLabelRenamerMock, 0),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, body),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectLabelUpdaterMock{}
				return ProjectLabelsPut(a), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(&a.ProjectAdderMock, 0),
					handler.HasCalls(&a.ProjectLabelRenamerMock, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, normalBody),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, normalBody),
		),
	)
}
