/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/store/storeuploads"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// The error-messages for creating a project
const (
	errCanNotAddProjectTitle = "Couldn't create project."
	errCanNotAddProject      = "We are sorry but we were unable to create the project as you requested." +
		"If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
)

type importProjectJson struct {
	Project   project.Project
	Cases     []test.Case
	Sequences []test.Sequence
}

// ProjectPost is used to save a new project in the system
func ProjectPost(pa handler.ProjectAdder, projectChecker id.ProjectExistenceChecker, ta handler.TestCaseAdder, tcg handler.TestCaseGetter, caseChecker id.TestExistenceChecker, tsa handler.TestSequenceAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		pn := r.FormValue(httputil.ProjectName)
		pd := r.FormValue(httputil.ProjectDescription)
		pvs := r.FormValue(httputil.ProjectVisibility)
		img := r.FormValue(httputil.ProjectImage)

		hasImport := len(r.FormValue(httputil.Import)) != 0

		var importedProject importProjectJson

		pv, err := visibility.StringToVis(pvs)
		if err != nil {
			errors.Handle(handler.InvalidVisibility(), w, r)
			return
		}

		c := handler.GetContextEntities(r)
		if c.User == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		p := project.NewProject(pn, c.User.ID(), pd, pv)
		if vErr := p.ID().Validate(projectChecker); vErr != nil {
			errors.Handle(vErr, w, r)
			return
		}

		addOwner(p, c)

		if hasImport {
			//Get Import Project as Json
			pi := r.FormValue(httputil.Import)
			b := []byte(pi)

			// Create Struct of Json import
			err := json.Unmarshal(b, &importedProject)

			if err != nil {
				errors.Handle(err, w, r)
			}

			// Import Roles
			p.Roles = importedProject.Project.Roles

			// Import Versions
			p.Versions = importedProject.Project.Versions

			// Import Labels
			p.Labels = importedProject.Project.Labels
		}

		// Set Project Image
		imagePath, err := storeuploads.WriteProjectImageToFile(img, p)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		p.Image = imagePath

		// Create Project
		err = pa.Add(&p)
		if err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				errCanNotAddProjectTitle, errCanNotAddProject, r).
				WithLog("Unable to add project to store.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		if hasImport {

			// Create Testcases with newest version from imported data
			for i := 0; i < len(importedProject.Cases); i++ {

				tc := creatTestCaseFromImport(importedProject, p, i)

				if err := addCase(ta, caseChecker, &tc); err != nil {
					errors.Handle(err, w, r)
					return
				}
			}

			// Creat Test Sequences from imported data
			for i := 0; i < len(importedProject.Sequences); i++ {

				ts, err := creatTestSequenceFromImport(importedProject, tcg, caseChecker, p, i)

				if err != nil {
					errors.Handle(err, w, r)
					return
				}
				tsa.Add(ts)
			}

		}

		// The header values are used to set the url with javascript
		httputil.SetHeaderValue(w, httputil.NewName, p.Name)
		httputil.SetHeaderValue(w, httputil.SignedInUser, c.User.Name)
	}
}

func creatTestSequenceFromImport(importedProject importProjectJson, tcg handler.TestCaseGetter, caseChecker id.TestExistenceChecker, p project.Project, i int) (*test.Sequence, error) {
	casesInTs := ""
	for j := 0; j < len(importedProject.Sequences[i].SequenceVersions[0].Cases); j++ {
		casesInTs = casesInTs + importedProject.Sequences[i].SequenceVersions[0].Cases[j].Name + "/"
	}

	// Remove / at the end of the String
	casesInTs = strings.TrimSuffix(casesInTs, "/")

	// Create Test Sequence Input form Imported Data
	ts, err := createTestSequence(tcg, caseChecker, &p, TestSequenceInput{
		InputTestSequenceName:          importedProject.Sequences[i].Name,
		InputTestSequenceDescription:   importedProject.Sequences[i].SequenceVersions[0].Description,
		InputTestSequencePreconditions: importedProject.Sequences[i].SequenceVersions[0].Preconditions,
		InputTestSequenceLabels:        importedProject.Sequences[i].Labels,
		InputTestSequenceTestCase:      casesInTs,
	})
	return ts, err
}

/**
Add owner as a member with the role "Supervisor" to the project p
*/
func addOwner(p project.Project, c *handler.ContextEntities) {
	// Add owner as a member with the role "Supervisor"
	p.UserMembers[c.User.ID()] = project.UserMembership{
		User:        c.User.ID(),
		Role:        "Owner",
		MemberSince: time.Now().UTC().Round(time.Second),
	}
}

/**
Creates test case from Imported Json on array position i of Cases ( newest Version of the test case)
*/
func creatTestCaseFromImport(importedProject importProjectJson, p project.Project, i int) test.Case {

	// Creat new Test case
	tc := test.NewTestCase(
		importedProject.Cases[i].Name,
		importedProject.Cases[i].TestCaseVersions[0].Description,
		importedProject.Cases[i].TestCaseVersions[0].Preconditions,
		importedProject.Cases[i].Labels,
		importedProject.Cases[i].TestCaseVersions[0].Versions,
		importedProject.Cases[i].TestCaseVersions[0].Duration,
		p.ID(),
	)

	// Add Test Steps
	tc.TestCaseVersions[0].Steps = importedProject.Cases[0].TestCaseVersions[0].Steps

	return tc
}
