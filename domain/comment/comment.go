/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package comment

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

type Comment struct {

	Id 			int64
	CreatedAt 	time.Time `xorm:"created"`
	UpdatedAt 	time.Time `xorm:"updated"`
	DeletedAt 	time.Time `xorm:"deleted"`

	// Foreign Key ----------|
	TestId       int64
	AuthorId	 string
	//-----------------------|

	Text         	string
	TestVersionNr 	int
	IsCase			bool
	IsEdit 			bool `xorm:"not null default 0"`

	Author       *user.User	`xorm:"-"`

	// The Author-Role is lazy-loaded
	AuthorRole   *project.Role `xorm:"-"`

	// The Requester is a hack so we can access the requesting User in the template, as the comments template is asynchronous
	Requester    *user.User `xorm:"-"`
}

