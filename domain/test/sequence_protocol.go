/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"reflect"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

/*SequenceExecutionProtocol contains all information relating to an execution of a test sequence itself.
Data referring to the single test cases are saved in the corresponding CaseExecutionProtocol
*/
type SequenceExecutionProtocol struct {
	TestVersion id.TestVersionID
	ProtocolNr  int
	Result      Result

	SUTVersion    string
	SUTVariant    string
	ExecutionDate time.Time
	UserName      string
	IsAnonymous   bool

	PreconditionResults    []PreconditionResult
	CaseExecutionProtocols []id.ProtocolID
	OtherNeededTime        duration.Duration
}

// NewSequenceExecutionProtocol creates a new execution protocol of a test sequence execution with the given information
func NewSequenceExecutionProtocol(sequenceProtocolLister ProtocolLister, preconditionResult []PreconditionResult, sequenceVersion id.TestVersionID,
	sutVariant string, sutVersion string, neededTime duration.Duration, userName string, isAnonymous bool) (SequenceExecutionProtocol, error) {

	newID, err := GetProtocolNr(sequenceProtocolLister, sequenceVersion)
	if err != nil {
		return SequenceExecutionProtocol{}, err
	}

	protocol := SequenceExecutionProtocol{
		ProtocolNr:  newID,
		TestVersion: sequenceVersion,
		Result:      NotAssessed,

		SUTVersion: sutVersion,
		SUTVariant: sutVariant,

		PreconditionResults:    preconditionResult,
		ExecutionDate:          time.Now().UTC().Round(time.Second),
		UserName:               userName,
		IsAnonymous:            isAnonymous,
		CaseExecutionProtocols: nil,
		OtherNeededTime:        neededTime,
	}
	return protocol, nil
}

//AddCaseExecutionProtocol adds an case protocol to the sequence protocol
func (s *SequenceExecutionProtocol) AddCaseExecutionProtocol(caseProtocol CaseExecutionProtocol) {
	s.CaseExecutionProtocols = append(s.CaseExecutionProtocols, caseProtocol.ID())
	s.updateResultNewCaseProtocol(caseProtocol)
}

//updateResult calculates and set the overall result of the sequenceProtocol out of all the contained case-results.
//The rules for the calculation:
//At least one Fail ->  fail
//No Fail, at least one PartiallySuccessful ->  PartiallySuccessful
//No Fail, at least one NotAssessed but not only ->  PartiallySuccessful
//Only Pass ->  Pass
//Only NotAssessed ->  NotAssessed
//Suppress linter "not used"
//nolint: megacheck
func (s *SequenceExecutionProtocol) UpdateResult(cpg CaseProtocolGetter) error {
	first := true

	if len(s.CaseExecutionProtocols) == 0 {
		s.Result = NotAssessed
		return nil
	}
	for _, caseProtocolID := range s.CaseExecutionProtocols {
		caseProtocol, err := cpg.GetCaseExecutionProtocol(caseProtocolID)
		if err != nil {
			return err
		}
		if first {
			first = false
			s.Result = caseProtocol.Result
		}
		s.updateResultNewCaseProtocol(caseProtocol)
	}
	return nil
}

//updateResultNewCaseProtocol recalculates and set the overall result of the sequenceProtocol considering only the
//old result and the given Protocol.
//This function does NOT recalculate the result from all containing caseProtocols!
//Therefor the result can't get any better, just worse if the result of the given caseProtocol is worse.
//The rules for the calculation:
//Given Protocol is the first in seq ->  caseResult
//Seq Fail OR case Fail ->  Fail
//Seq Pass AND Case Pass -> Pass
//Seq NotAssessed AND Case NotAssessed -> NotAssessed
//else ->  PartiallySuccessful
func (s *SequenceExecutionProtocol) updateResultNewCaseProtocol(protocol CaseExecutionProtocol) {
	if len(s.CaseExecutionProtocols) == 0 ||
		(len(s.CaseExecutionProtocols) == 1 && s.CaseExecutionProtocols[0] == protocol.ID()) {
		s.Result = protocol.Result
		return
	}
	if s.Result == Fail || protocol.Result == Fail {
		s.Result = Fail
		return
	}
	if s.Result == Pass && protocol.Result == Pass {
		s.Result = Pass
		return
	}
	if s.Result == NotAssessed && protocol.Result == NotAssessed {
		s.Result = NotAssessed
		return
	}
	s.Result = PartiallySuccessful
}

// Equals compares two SequenceExecutionProtocol and returns true, if these two are equal.
// The contained CaseExecutionProtocols will be compared too.
// If in is nil, Equals will return false.
func (s SequenceExecutionProtocol) Equals(in interface{}) bool {
	return reflect.DeepEqual(s, in)
}

// ID returns a caseProtocols id
func (prt CaseExecutionProtocol) ID() id.ProtocolID {
	return id.NewProtocolID(prt.TestVersion, prt.ProtocolNr)
}

// ID returns a sequenceProtocols id
func (s SequenceExecutionProtocol) ID() id.ProtocolID {
	return id.NewProtocolID(s.TestVersion, s.ProtocolNr)
}
