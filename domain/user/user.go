/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package user

import (
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

// The User struct contains information to represent a user in the system
type User struct {
	DisplayName      string
	Name             string
	Email            string
	RegistrationDate time.Time
	IsAdmin          bool
	Biography        string
	IsShownPublic    bool
	IsEmailPublic    bool
}

// PasswordUser is a user that contains a user with
// a password.
// This type is primarily used in the user store to
// authenticate or register a user. This reduces the
// risk of the password getting accidentally to
// places it does not belong
type PasswordUser struct {
	User
	Password string
}

// New creates a new user with the given display name,
// account name and email address.
// Returns the created user.
func New(display, name, email string, isAdmin bool) User {
	display = strings.TrimSpace(display)
	name = strings.TrimSpace(name)
	email = strings.TrimSpace(email)

	return User{
		Name:             name,
		DisplayName:      display,
		Email:            email,
		RegistrationDate: time.Now().UTC().Round(time.Second),
		IsAdmin:          isAdmin,
		Biography:        "",
		IsShownPublic:    true,
		IsEmailPublic:    false,
	}
}

//ID returns the ID struct identifying this user
func (u User) ID() id.ActorID {
	return id.NewActorID(u.Name)
}
