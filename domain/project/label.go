/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"strings"
)

// A Label is used to group tests
type Label struct {
	Name        string
	Description string
	Color       string
}

// NewLabel creates a new label
func NewLabel(name, description, color string) Label {
	name = strings.TrimSpace(name)
	description = strings.TrimSpace(description)

	l := Label{
		Name:        name,
		Description: description,
		Color:       color,
	}

	return l
}

/*
//toStrings is a helper function for encoding. Returns all fields as a string slice
func (label *Label) toStrings() []string {
	return []string{
		label.Name,
		label.Description,
		label.Color,
	}
}

//fromStrings is a helper function for decoding. Fills the fields with the information given in the string slice
func (label *Label) fromStrings(strings []string) (err error) {

	label.Name = strings[0]
	label.Description = strings[1]
	label.Color = strings[2]
	if err != nil {
		return
	}
	return

}

//GobEncode encodes the id to a byte stream so it can be stored for example to a session
func (label *Label) GobEncode() ([]byte, error) {
	idString := strings.Join(label.toStrings(), "?")
	return []byte(idString), nil
}

//GobDecode decodes the id from a byte stream. All fields of the id called on will be overwritten
func (label *Label) GobDecode(data []byte) (err error) {
	labelString := string(data)
	labels := strings.Split(labelString, "?")
	return label.fromStrings(labels)
}
*/