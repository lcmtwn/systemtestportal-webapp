/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

//---------------------------------------------------------------------------------------------------------------------|
// The systemsettings are a Automated XORM-struct. This means it is synced directly to the database.
//---------------------------------------------------------------------------------------------------------------------|

package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/contextdomain"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/system"
)

type SystemSettingsSQL struct {
	engine *xorm.Engine
}

func (SystemSettingsSQL SystemSettingsSQL) GetSystemSettings() (*system.SystemSettings, error) {
	var systemSettings system.SystemSettings

	_, err := SystemSettingsSQL.engine.Get(&systemSettings)
	if err != nil {
		return nil, err
	}

	return &systemSettings, nil
}

func (SystemSettingsSQL SystemSettingsSQL) UpdateSystemSettings(systemSettings *system.SystemSettings) error {
	// XORMs update function ignores booleans by default ..... so .UseBool required
	_, err := SystemSettingsSQL.engine.ID(1).UseBool().Update(systemSettings)
	if err != nil {
		return err
	}

	contextdomain.SetGlobalSystemSettings(systemSettings)
	return nil
}

func (SystemSettingsSQL SystemSettingsSQL) InsertSystemSettings(systemSettings *system.SystemSettings) error {
	_, err := SystemSettingsSQL.engine.Insert(systemSettings)
	if err != nil {
		return err
	}

	contextdomain.SetGlobalSystemSettings(systemSettings)
	return nil
}
