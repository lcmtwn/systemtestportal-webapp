// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package dummydata

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

// Cases contains all dummy cases for
// the default configuration of the system
var Cases = []test.Case{
	{
		Project: Projects[0].ID(),
		Name:    "Change Theme",

		Labels: []project.Label{
			{
				Name:        "High Priority",
				Description: "This test has a high priority",
				Color:       "ff0000",
			},
			{
				Name:        "Low Priority",
				Description: "This test has a low priority",
				Color:       "ff7b00",
			},
		},

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Change Theme", true),
				VersionNr: 1,

				Description:   "Change the theme of the user interface",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened. The third theme is not selected in the settings."}},

				Duration: duration.NewDuration(0, 5, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Type \"SystemTestPortal\" into the search bar and press enter.",
						ExpectedResult: "The search results for \"SystemTestPortal\" appear in a list.",
					},
					{
						Index:          2,
						Action:         "Click on the menu-button (The three horizontal lines at the top right corner).",
						ExpectedResult: "The sidebar opens.",
					},
					{
						Index:  3,
						Action: "Click on the third theme (the blue-green one).",
						ExpectedResult: "The user interface design changes. The URL addresses are displayed beneath " +
							"the title of the search result. The URL font color changes to green. The icons before " +
							"the URL disappear. The font gets bold.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Search for Websites",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Websites", true),
				VersionNr: 1,

				Description:   "A default keyword-search",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened. The third theme is not selected in the settings."}},

				Duration: duration.NewDuration(0, 5, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
					"Android": {
						Name: "Android",
						Variants: []project.Variant{
							{Name: "0.8.0"},
							{Name: "4.4.0"},
							{Name: "4.4.1"},
							{Name: "5.3.1"},
						},
					},
					"iOS": {
						Name: "iOS",
						Variants: []project.Variant{
							{Name: "0.22.0"},
							{Name: "7.5.0.0"},
							{Name: "7.5.2.0"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: "Type \"SystemTestPortal\" into the search bar and press enter.",
						ExpectedResult: `"SystemTestPortal" is written into the search bar. The search results are ` +
							`listed. Under the search bar "Web" is selected as search result type.`,
					},
					{
						Index:          2,
						Action:         "Click on the search result that leads to systemtestportal.org.",
						ExpectedResult: "www.systemtestportal.org opens.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Search for Images",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Images", true),
				VersionNr: 1,

				Description:   "A image search",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened. The third theme is not selected in the settings."}},

				Duration: duration.NewDuration(0, 5, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
					"Android": {
						Name: "Android",
						Variants: []project.Variant{
							{Name: "4.4.0"},
							{Name: "4.4.1"},
							{Name: "5.3.1"},
						},
					},
					"iOS": {
						Name: "iOS",
						Variants: []project.Variant{
							{Name: "7.5.0.0"},
							{Name: "7.5.2.0"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: "Type \"portal image\" into the search bar and press enter.",
						ExpectedResult: `A new layout opens with multiple images. Under the search bar "Images" is ` +
							`selected as search result type.`,
					},
					{
						Index: 2,
						Action: "Under the row to select the search result type there is a second row of filters. " +
							"Click on the last filter which says \"All Colors\" and select \"Black and White\".",
						ExpectedResult: "Instead of \"All Color\" there stands now \"Black and White\". " +
							"The search results change. All search results are predominantly black and white.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Search for Definitions",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Definitions", true),
				VersionNr: 1,

				Description:   "A search for a definition",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened. The third theme is not selected in the settings."}},

				Duration: duration.NewDuration(0, 3, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
					"Android": {
						Name: "Android",
						Variants: []project.Variant{
							{Name: "0.8.0"},
							{Name: "4.4.0"},
							{Name: "4.4.1"},
							{Name: "5.3.1"},
						},
					},
					"iOS": {
						Name: "iOS",
						Variants: []project.Variant{
							{Name: "0.22.0"},
							{Name: "7.5.0.0"},
							{Name: "7.5.2.0"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: "Type \"test definition\" into the search bar and press enter.",
						ExpectedResult: "Under the search bar \"Definition\" is selected as search result type. " +
							"On the top is a highlighted area with definitions for \"test\". " +
							"Several search results of websites follow. ",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Change Language",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Change Language", true),
				VersionNr: 1,

				Description: "Change the language settings",
				Preconditions: []test.Precondition{
					test.Precondition{
						Content: "DuckDuckGo.com is opened."},
					test.Precondition{
						Content: "\"Español de España\" is not selected in the language settings of DuckDuckGo.com."},
				},

				Duration: duration.NewDuration(0, 7, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
					"iOS": {
						Name: "iOS",
						Variants: []project.Variant{
							{Name: "7.5.0.0"},
							{Name: "7.5.2.0"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the menu-button (The three horizontal lines at the top right corner).",
						ExpectedResult: "The sidebar opens.",
					},
					{
						Index:          2,
						Action:         `Select "other Settings".`,
						ExpectedResult: "A list of adjustable settings is viewed.",
					},
					{
						Index:          3,
						Action:         `Select "Español de España" in the drop down menu on the right of "Language".`,
						ExpectedResult: `The site is translated into Spanish (e.g. Settings changes into "Ajustes").`,
					},
					{
						Index:  4,
						Action: `Scroll to the end of the settings list and click "Guardar y salir".`,
						ExpectedResult: "The site changes back to DuckDuckGo.com. The language viewed here is the " +
							"language you selected in your browser settings.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Install DuckDuckGo.com for Chrome",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Install DuckDuckGo.com for Chrome", true),
				VersionNr: 1,

				Description:   "Add DuckDuckGo.com as a search engine to your browser",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},

				Duration: duration.NewDuration(0, 8, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -2),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the arrow down in the bottom center. ",
						ExpectedResult: "The page scrolls down.",
					},
					{
						Index:          2,
						Action:         "Click on the button \"Add DuckDuckGo to Chrome\".",
						ExpectedResult: "The notification \"Add DuckDuckGo for Chrome?\" appears. ",
					},
					{
						Index:  3,
						Action: "Click \"Add extension\". ",
						ExpectedResult: `The notification "DuckDuckGo for Chrome has been added to Chrome..." ` +
							`appears. In the top right corner the icon of DuckDuckGo is viewed.`,
					},
					{
						Index:          4,
						Action:         "Press ALT + G.",
						ExpectedResult: "In the top right corner a pop up appears with a search bar inside.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Change Font Size",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Change Font Size", true),
				VersionNr: 1,

				Description: "Change the font size of the search engine",
				Preconditions: []test.Precondition{
					test.Precondition{
						Content: "DuckDuckGo.com is opened."},
					test.Precondition{
						Content: "Large is selected as font size in the settings."},
				},

				Duration: duration.NewDuration(0, 7, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
					"Android": {
						Name: "Android",
						Variants: []project.Variant{
							{Name: "0.8.0"},
							{Name: "4.4.0"},
							{Name: "4.4.1"},
							{Name: "5.3.1"},
						},
					},
					"iOS": {
						Name: "iOS",
						Variants: []project.Variant{
							{Name: "0.22.0"},
							{Name: "7.5.0.0"},
							{Name: "7.5.2.0"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the menu-button (The three horizontal lines at the top right corner).",
						ExpectedResult: "The sidebar opens.",
					},
					{
						Index:          2,
						Action:         `Select "other Settings".`,
						ExpectedResult: "A list of adjustable settings is viewed.",
					},
					{
						Index:          3,
						Action:         `Click "Appearance" in the list on the right of the headline "Settings".`,
						ExpectedResult: "A list of other adjustable settings is viewed.",
					},
					{
						Index:          4,
						Action:         `Select "Largest" in the drop down menu on the right of "Font Size".`,
						ExpectedResult: "The font size increases.",
					},
					{
						Index:          5,
						Action:         `Scroll to the end of the settings list and click "Save and Exit".`,
						ExpectedResult: "The site changes back to DuckDuckGo.com.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Map",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Map", true),
				VersionNr: 1,

				Description:   "Search a place and view it on a map",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},

				Duration: duration.NewDuration(0, 5, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
					"Android": {
						Name: "Android",
						Variants: []project.Variant{
							{Name: "0.8.0"},
							{Name: "4.4.0"},
							{Name: "4.4.1"},
							{Name: "5.3.1"},
						},
					},
					"iOS": {
						Name: "iOS",
						Variants: []project.Variant{
							{Name: "0.22.0"},
							{Name: "7.5.0.0"},
							{Name: "7.5.2.0"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: `Type "Stuttgart Map" into the search bar.`,
						ExpectedResult: "The search results are viewed. At the top is a small map showing stuttgart. " +
							"A list of websites follows.",
					},
					{
						Index:          2,
						Action:         `Select "OpenStreetMap" in the drop down in the bottom right corner of the map.`,
						ExpectedResult: "OpenStreetMap is written next to the small triangle of the drop down menu.",
					},
					{
						Index:          3,
						Action:         "Click on the map.",
						ExpectedResult: "The map opens in full screen mode.",
					},
					{
						Index:  4,
						Action: `Click on "Directions".`,
						ExpectedResult: "OpenStreetMap opens. The coordinates 48.7761, 9.1775 are entered as the " +
							"finish of the route.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Enable Autosuggestion",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Enable Autosuggestion", true),
				VersionNr: 1,

				Description:   "Enable autosuggestion in your settings",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},

				Duration: duration.NewDuration(0, 6, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v1018"},
						},
					},
					"Android": {
						Name: "Android",
						Variants: []project.Variant{
							{Name: "5.3.1"},
						},
					},
					"iOS": {
						Name: "iOS",
						Variants: []project.Variant{
							{Name: "7.5.2.0"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         `Type "s" into the search bar.`,
						ExpectedResult: "No list of auto-suggestions appears under the search bar.",
					},
					{
						Index:          2,
						Action:         "Click on the menu-button (The three horizontal lines at the top right corner).",
						ExpectedResult: "The sidebar opens.",
					},
					{
						Index:          3,
						Action:         `Select "other Settings".`,
						ExpectedResult: "A list of adjustable settings is viewed.",
					},
					{
						Index:          4,
						Action:         `Click the button "Off" next to "Auto-Suggest".`,
						ExpectedResult: `The color of the button changes. The description of the button changes to "On".`,
					},
					{
						Index:          5,
						Action:         `Scroll to the end of the settings list and click "Save and Exit".`,
						ExpectedResult: "The site changes back to DuckDuckGo.com.",
					},
					{
						Index:          6,
						Action:         `Type "s" into the search bar.`,
						ExpectedResult: "A list of auto-suggestions appears under the search bar.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Search for Recipes",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Recipes", true),
				VersionNr: 2,

				Description:   "A search for recipes",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},

				Duration: duration.NewDuration(0, 3, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
					"Android": {
						Name: "Android",
						Variants: []project.Variant{
							{Name: "0.8.0"},
							{Name: "4.4.0"},
							{Name: "4.4.1"},
							{Name: "5.3.1"},
						},
					},
					"iOS": {
						Name: "iOS",
						Variants: []project.Variant{
							{Name: "0.22.0"},
							{Name: "7.5.0.0"},
							{Name: "7.5.2.0"},
						},
					},
				},
				Message:      "Correcting a spelling mistake",
				IsMinor:      true,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: `Type "Pizza recipes" into the search bar and press enter.`,
						ExpectedResult: `"Pizza recipes" is written into the search bar. The search results are ` +
							`listed. Under the search bar "Recipes" is selected as search result type.`,
					},
				},
			},
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Recipes", true),
				VersionNr: 1,

				Description:   "A search for receipts",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},

				Duration: duration.NewDuration(0, 3, 0),
				Versions: map[string]*project.Version{
					"Chrome": {
						Name: "Chrome",
						Variants: []project.Variant{
							{Name: "v0.2"},
							{Name: "v0.3.1"},
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Variants: []project.Variant{
							{Name: "v0.1.11"},
							{Name: "v0.2.49"},
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					},
					"Microsoft Edge": {
						Name: "Microsoft Edge",
						Variants: []project.Variant{
							{Name: "v0.175"},
							{Name: "v0.204"},
							{Name: "v1000"},
							{Name: "v1019"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: `Type "Pizza recipes" into the search bar and press enter.`,
						ExpectedResult: `"Pizza recipes" is written into the search bar. The search results are ` +
							`listed. Under the search bar "Recipes" is selected as search result type.`,
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Install DuckDuckGo.com for Firefox",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Install DuckDuckGo.com for Firefox", true),
				VersionNr: 1,

				Description:   "Add DuckDuckGo.com as a search engine to your browser",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},

				Duration: duration.NewDuration(0, 3, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().Add(-time.Hour * 2),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the arrow down in the bottom center. ",
						ExpectedResult: "The page scrolls down.",
					},
					{
						Index:          2,
						Action:         `Click on the button "Add DuckDuckGo to Firefox".`,
						ExpectedResult: `The notification "Firefox blocked this website...." appears.`,
					},
					{
						Index:          3,
						Action:         `Click "Permit".`,
						ExpectedResult: `The notification "Add DuckDuckGo Plus? ..." appears.`,
					},
					{
						Index:  4,
						Action: `Click "Add".`,
						ExpectedResult: `The notification "DuckDuckGo for Chrome has been added to Chrome..." ` +
							`appears. In the top right corner the icon of DuckDuckGo is viewed.`,
					},
					{
						Index:          5,
						Action:         "Press ALT + G.",
						ExpectedResult: "In the top right corner a pop up appears with a search bar inside.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Install DuckDuckGo.com for Microsoft Edge",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Install DuckDuckGo.com for Microsoft Edge", true),
				VersionNr: 1,

				Description:   "Add DuckDuckGo.com as a search engine to your browser",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},

				Duration: duration.NewDuration(0, 3, 0),
				Versions: map[string]*project.Version{
					"Desktop": {
						Name: "Desktop",
						Variants: []project.Variant{
							{Name: "v512"},
							{Name: "v777"},
							{Name: "v902"},
							{Name: "v1018"},
						},
					},
				},
				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().UTC().Add(-time.Hour * 2),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the arrow down in the bottom center. ",
						ExpectedResult: "The page scrolls down.",
					},
					{
						Index:          2,
						Action:         `Click on the button "Add DuckDuckGo to Edge".`,
						ExpectedResult: `The site "Take Back Your Privacy! ..." is displayed.`,
					},
					{
						Index: 3,
						Action: `Follow the instructions on this site. Open a new tap and type "SystemTestPortal" ` +
							`into the search bar.`,
						ExpectedResult: `The search engine you used was DuckDuckGo (i.e the URL in the search bar ` +
							`starts with "https://duckduckgo.com/").`,
					},
				},
			},
		},
	},
	{
		Name:    "Sign in",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "A user signs in.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click the \"Sign In\" button.",
						ExpectedResult: "The sign in modal opens.",
					},
					{Action: "Enter an e-mail address and password and click \"Sign In\".",
						ExpectedResult: "The \"Sign In\" button from earlier disappeared. " +
							"The user is now able to work on public projects (e.g. edit test cases).", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Sign in", true),
			},
		},
	},
	{
		Name:    "Sign out",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "A user signs out.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click on your display name in the top right corner.",
						ExpectedResult: "A context menue opens.", Index: 1},
					{Action: "Click \"Sign out\" in the context menu.",
						ExpectedResult: "Your display name is replaced by a \"Sign in\" button." +
							" The user isn't able to work on any projects (e.g. edit test cases).", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Sign out", true),
			},
		},
	},
	{
		Name:    "Registration",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "A user gets registrated.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click on \"Add new user\".",
						ExpectedResult: "The site \"Create new User\" opens.", Index: 1},
					{Action: "Enter user name, display name, e-mail address, password and repeat" +
						" the password. Click Register",
						ExpectedResult: "The user is now able to sign in with the inserted information above.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Registration", true),
			},
		},
	},
	{
		Name:    "Create a Project",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "A new project is created.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click on \"+ New Project\".",
						ExpectedResult: "A new page opens.", Index: 1},
					{Action: "Enter project name and project description." +
						" Click \"Create Project\".",
						ExpectedResult: "A list of all visible projects is viewed." +
							" The project created above is in this list.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Create a Project", true),
			},
		},
	},
	{
		Name:    "Comment on a test case",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "Create a new comment.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Enter a new comment. Click \"Comment\".",
						ExpectedResult: "The comment is displayed below. ", Index: 1},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Comment on a test case", true),
			},
		},
	},
	{
		Name:    "Comment on a test sequence",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "Create a new comment.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Enter a new comment. Click \"Comment\".",
						ExpectedResult: "The comment is displayed below.", Index: 1},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Comment on a test sequence", true),
			},
		},
	},
	{
		Name:    "Create test case",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "A new test case is created.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click \"+ New Test Case\".",
						ExpectedResult: "The page \"Create Test Case\" opens.", Index: 1},
					{Action: "Enter test case name, description and conditions, " +
						"add test steps and select Variants, Versions, estimated time and labels. Click \"Save\".",
						ExpectedResult: "A list of all test cases of the project is viewed." +
							"The test case created above is in this list.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Create test case", true),
			},
		},
	},
	{
		Name:    "Add test steps",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "A new test step is added to an existing test case.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click \"Add Test Step\".",
						ExpectedResult: "The test step modal opens.", Index: 1},
					{Action: "Enter a task and description. Click \"OK\".",
						ExpectedResult: "The modal closes. The new test step is viewed in the list of test steps.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Add test steps", true),
			},
		},
	},
	{
		Name:    "Edit a test case",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "An existing test case is edited.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click \"Edit\".",
						ExpectedResult: "The test step modal opens.", Index: 1},
					{Action: "Make some changes to the test case and click \"Save\".",
						ExpectedResult: "A modal opens.", Index: 2},
					{Action: "Fill out the modal and click \"Save\".",
						ExpectedResult: "The test case is viewed with the changes.", Index: 3},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Edit a test case", true),
			},
		},
	},
	{
		Name:    "Execute a test case",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "An existing test case is executed.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click \"Start\".",
						ExpectedResult: "The step-for-step execution starts.", Index: 1},
					{Action: "Follow this execution and click \"Finish\".",
						ExpectedResult: "For each test step and for the overall result \"not assesed\"" +
							", \"failed\", \"PartiallySuccessful\" and \"passed\" can be selected. " +
							"A protocol of the execution is created.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Execute a test case", true),
			},
		},
	},
	{
		Name:    "Create test sequence",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "Group multiple test cases in one test sequence.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click \"+ New Test Sequence\".",
						ExpectedResult: "The page \"Create Test Sequence\" opens.", Index: 1},
					{Action: "Enter test sequence name, description and conditions, " +
						"add test cases and select labels. Click \"Save\".",
						ExpectedResult: "A list of all test sequences of the project is viewed." +
							"The test sequence created above is in this list.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Create test sequence", true),
			},
		},
	},
	{
		Name:    "Assign a test case",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "Assign a test case.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click \"Assign\".",
						ExpectedResult: "The assign modal opens.", Index: 1},
					{Action: "Select the user you want to assign the test case to. Click \"OK\".",
						ExpectedResult: "The selected users can see this assignment in their Task list.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Assign a test case", true),
			},
		},
	},
	{
		Name:    "Assign a test sequence",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "Assign a test sequence.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click \"Assign\".",
						ExpectedResult: "The assign modal opens.", Index: 1},
					{Action: "Select the user you want to assign the test sequence to. Click \"OK\".",
						ExpectedResult: "The selected users can see this assignment in their Task list.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Assign a test sequence", true),
			},
		},
	},
	{
		Name:    "View protocols",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "View the details of a protocol.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "If the protocol is from a test case execution select \"Test Cases\"" +
						" otherwise \"Test Sequences\". Select the name of the executed test case or " +
						"test sequence in the drop down.",
						ExpectedResult: "A list of all protocols of the selected test case or test sequence is viewed.", Index: 1},
					{Action: "Select the protocol you want to see.",
						ExpectedResult: "The results, comments and notes of the execution are viewed.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "view protocols", true),
			},
		},
	},
	{
		Name:    "time execution",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "The time of an test execution is stopped.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click \"Start\".",
						ExpectedResult: "The step-by-step execution and the timer start.", Index: 1},
					{Action: "Pause the timer.",
						ExpectedResult: "The timer stops counting.", Index: 2},
					{Action: "Continue the timer.",
						ExpectedResult: "The timer continues counting.", Index: 3},
					{Action: "Continue the execution and finish it.",
						ExpectedResult: "The timer stopps. The execution time is saved with the test protocol.", Index: 4},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "time execution", true),
			},
		},
	},
	{
		Name:    "Versioning",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "Test cases are versioned.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click \"History\".",
						ExpectedResult: "The history of the test is viewed.", Index: 1},
					{Action: "Click on an older version.",
						ExpectedResult: "The older version of the test ist viewed.", Index: 2},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Versioning", true),
			},
		},
	},
	{
		Name:    "Print",
		Project: Projects[1].ID(),
		TestCaseVersions: []test.CaseVersion{
			0: {
				VersionNr:     1,
				Message:       "Initial test case created",
				IsMinor:       false,
				Description:   "Print a test sequence, test case, protocol or a list of them.",
				Preconditions: []test.Precondition{test.Precondition{Content: "DuckDuckGo.com is opened"}},
				Duration:      duration.NewDuration(0, 5, 0),
				Versions:      stpVariants,
				Steps: []test.Step{
					{Action: "Click on the print button.",
						ExpectedResult: "A print preview opens in a separate window.", Index: 1},
					{Action: "Click \"Print\".",
						ExpectedResult: "The settings of the pushing operation appear.", Index: 2},
					{Action: "Adjust the settings of the pushing operation and print the document.",
						ExpectedResult: "The selected document is printed.", Index: 3},
				},
				CreationDate: time.Now().AddDate(0, 0, -1),
				Case:         id.NewTestID(Projects[1].ID(), "Print", true),
			},
		},
	},
}
