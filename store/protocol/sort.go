/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package protocol

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type byExecutionDate []test.CaseExecutionProtocol
type byExecutionDate_Sequences []test.SequenceExecutionProtocol

func (bed byExecutionDate) Len() int {
	return len(bed)
}

func (bed byExecutionDate) Less(i, j int) bool {
	return bed[i].ExecutionDate.Before(bed[j].ExecutionDate)
}

func (bed byExecutionDate) Swap(i, j int) {
	bed[i], bed[j] = bed[j], bed[i]
}

func (bed byExecutionDate_Sequences) Len() int {
	return len(bed)
}

func (bed byExecutionDate_Sequences) Less(i, j int) bool {
	return bed[i].ExecutionDate.Before(bed[j].ExecutionDate)
}

func (bed byExecutionDate_Sequences) Swap(i, j int) {
	bed[i], bed[j] = bed[j], bed[i]
}
