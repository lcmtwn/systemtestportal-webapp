// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package storeuploads

import (
	"testing"
)

const (
	testImageName   = "DuckDuckGo.com...STP TestplanUltimate Comparison Framework"
	testImagePrefix = "data:image/svg+xml;base64,"
	testImageSuffix = "PCEtLQpUaGlzIGZpbGUgaXMgcGFydCBvZiBTeXN0ZW1UZXN0UG9ydGFsLgpDb3B5cmlnaHQgKEMpIDIwMTcgIEluc3Rpd" +
		"HV0ZSBvZiBTb2Z0d2FyZSBUZWNobm9sb2d5LCBVbml2ZXJzaXR5IG9mIFN0dXR0Z2FydAoKU3lzdGVtVGVzdFBvcnRhbCBpcyBmcmVlIHNv" +
		"ZnR3YXJlOiB5b3UgY2FuIHJlZGlzdHJpYnV0ZSBpdCBhbmQvb3IgbW9kaWZ5Cml0IHVuZGVyIHRoZSB0ZXJtcyBvZiB0aGUgR05VIEdlbmV" +
		"yYWwgUHVibGljIExpY2Vuc2UgYXMgcHVibGlzaGVkIGJ5CnRoZSBGcmVlIFNvZnR3YXJlIEZvdW5kYXRpb24sIGVpdGhlciB2ZXJzaW9uID" +
		"Mgb2YgdGhlIExpY2Vuc2UsIG9yCihhdCB5b3VyIG9wdGlvbikgYW55IGxhdGVyIHZlcnNpb24uCgpTeXN0ZW1UZXN0UG9ydGFsIGlzIGRpc" +
		"3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsCmJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBl" +
		"dmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mCk1FUkNIQU5UQUJJTElUWSBvciBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRS4" +
		"gIFNlZSB0aGUKR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy4KCllvdSBzaG91bGQgaGF2ZSByZWNlaXZlZC" +
		"BhIGNvcHkgb2YgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlCmFsb25nIHdpdGggU3lzdGVtVGVzdFBvcnRhbC4gIElmIG5vdCwgc" +
		"2VlIDxodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvPi4KLS0+Cgo8c3ZnIHdpZHRoPSI2MDAiIGhlaWdodD0iNjAwIiB4bWxucz0iaHR0" +
		"cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogPCEtLSBDcmVhdGVkIHdpdGggTWV0aG9kIERyYXcgLSBodHRwOi8vZ2l0aHViLmNvbS9kdW9" +
		"waXhlbC9NZXRob2QtRHJhdy8gLS0+CiA8ZGVmcz4KICA8ZmlsdGVyIGlkPSJzdmdfNl9ibHVyIj4KICAgPGZlR2F1c3NpYW5CbHVyIHN0ZE" +
		"RldmlhdGlvbj0iMC4xIiBpbj0iU291cmNlR3JhcGhpYyIvPgogIDwvZmlsdGVyPgogPC9kZWZzPgogPGc+CiAgPHRpdGxlPmJhY2tncm91b" +
		"mQ8L3RpdGxlPgogIDxyZWN0IGZpbGw9IiM1OWE5ZmYiIGlkPSJjYW52YXNfYmFja2dyb3VuZCIgaGVpZ2h0PSI2MDIiIHdpZHRoPSI2MDIi" +
		"IHk9Ii0xIiB4PSItMSIvPgogIDxnIGRpc3BsYXk9Im5vbmUiIG92ZXJmbG93PSJ2aXNpYmxlIiB5PSIwIiB4PSIwIiBoZWlnaHQ9IjEwMCU" +
		"iIHdpZHRoPSIxMDAlIiBpZD0iY2FudmFzR3JpZCI+CiAgIDxyZWN0IGZpbGw9InVybCgjZ3JpZHBhdHRlcm4pIiBzdHJva2Utd2lkdGg9Ij" +
		"AiIHk9IjAiIHg9IjAiIGhlaWdodD0iMTAwJSIgd2lkdGg9IjEwMCUiLz4KICA8L2c+CiA8L2c+CiA8Zz4KICA8dGl0bGU+TGF5ZXIgMTwvd" +
		"Gl0bGU+CiAgPHJlY3QgZmlsdGVyPSJ1cmwoI3N2Z182X2JsdXIpIiByeD0iMTAwIiBpZD0ic3ZnXzYiIGhlaWdodD0iNDIwIiB3aWR0aD0i" +
		"NDIwIiB5PSI5MCIgeD0iOTAiIHN0cm9rZS13aWR0aD0iNCIgc3Ryb2tlPSIjZmZmZmZmIiBmaWxsPSIjMDA3YmZmIi8+CiAgPHRleHQgZm9" +
		"udC1zdHlsZT0ibm9ybWFsIiBmb250LXdlaWdodD0ibm9ybWFsIiB4bWw6c3BhY2U9InByZXNlcnZlIiB0ZXh0LWFuY2hvcj0ic3RhcnQiIG" +
		"ZvbnQtZmFtaWx5PSJIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmIiBmb250LXNpemU9IjE3MiIgaWQ9InN2Z185IiB5PSIzNjAiIHg9I" +
		"jI0Mi42NDA2MjUiIHN0cm9rZS13aWR0aD0iMCIgc3Ryb2tlPSIjZmZmZmZmIiBmaWxsPSIjZmZmZmZmIj5QPC90ZXh0PgogPC9nPgo8L3N2" +
		"Zz4="

	methodEscape                                   = "escape()"
	methodTransformBase64ToFileContentAndExtension = "transformBase64ToFileContentAndExtension()"
	methodTestGetProjectImageFromPath              = "testGetProjectImageFromPath()"
)

func TestGetProjectImagePath(t *testing.T) {

	var existingImage = GetProjectImagePath("../../static/img/STP-Logo.png")
	if existingImage != "../../static/img/STP-Logo.png" {
		t.Error(methodTestGetProjectImageFromPath + " not able to resolve existing image '" +
			existingImage + "'.")
	}

	var notExistingImage = GetProjectImagePath("xDxDxD.png")
	if notExistingImage != PathPlaceholderImage {
		t.Error(methodTestGetProjectImageFromPath + " should not be able to find image '" +
			notExistingImage + "'.")
	}
}

func TestEscape(t *testing.T) {
	var escaped = escape(testImageName)

	if escaped != "DuckDuckGo-com---STP TestplanUltimate Comparison Framework" {
		t.Error(methodEscape + "is not escaping properly. Expected " +
			"'DuckDuckGo-com---STP TestplanUltimate Comparison Framework', got '" + escaped + "'.")
	}
}

func TestTransformBase64ToFileTypeAndContentImageContent(t *testing.T) {

	var base = testImagePrefix + testImageSuffix

	content, _ := transformBase64ToFileContentAndExtension(base)

	if content != testImageSuffix {
		t.Error(methodTransformBase64ToFileContentAndExtension + " changed the base64 string which is not allowed!")
	}

}

func TestTransformBase64ToFileTypeAndContentImageExtensions(t *testing.T) {

	var gif = "data:image/gif;base64,"
	_, extension := transformBase64ToFileContentAndExtension(gif)
	if extension != "gif" {
		t.Error(methodTransformBase64ToFileContentAndExtension + " is not able to handle gif images. " +
			"Expected 'gif', got '" + extension + "'.")
	}

	var jpg = "data:image/jpg;base64,"
	_, extension = transformBase64ToFileContentAndExtension(jpg)
	if extension != "jpg" {
		t.Error(methodTransformBase64ToFileContentAndExtension + " is not able to handle jpg images. " +
			"Expected 'jpg', got '" + extension + "'.")
	}

	var png = "data:image/png;base64,"
	_, extension = transformBase64ToFileContentAndExtension(png)
	if extension != "png" {
		t.Error(methodTransformBase64ToFileContentAndExtension + " is not able to handle png images. " +
			"Expected 'png', got '" + extension + "'.")
	}

	var svg = "data:image/svg+xml;base64,"
	_, extension = transformBase64ToFileContentAndExtension(svg)
	if extension != "svg" {
		t.Error(methodTransformBase64ToFileContentAndExtension + " is not able to handle svg images. " +
			"Expected 'svg', got '" + extension + "'.")
	}
}
