-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO project_versions (id, project_id, name)
VALUES
    ( 1, 2, 'Windows 32bit'),
    ( 2, 2, 'Windows 64bit'),
    ( 3, 2, 'Linux 32bit'),
    ( 4, 2, 'Linux 64bit'),
    ( 5, 1, 'Windows'),
    ( 6, 3, 'Windows XP'),
    ( 7, 3, 'Windows 7'),
    ( 8, 1, 'Linux'),
    ( 9, 3, 'Windows 8'),
    (10, 3, 'Windows 10'),
    (11, 1, 'Mac OS');

-- +migrate Down
DELETE FROM project_variants;