{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{/* Content for the opened task items*/}}
{{define "task-content" }}
<ul class="list-group list-group-flush">
    {{with .TaskList}}

        {{ range .GetTaskUndone }}

            <li class="list-group-item align-items-center" id="task-item-{{ .Index }}">
                <div class="d-flex">

                    {{/*<!-- Profile picture placeholder + Author name -->*/}}
                    {{/*<div>*/}}
                        {{/*<img src="/static/img/placeholder.svg" alt="..." class="rounded-circle mr-2" height="42" width="42">*/}}
                        {{/*<a href="/{{ .Author }}">*/}}
                            {{/*{{ .Author }}*/}}
                        {{/*</a>*/}}
                    {{/*</div>*/}}

                    {{/*<!-- Text -->*/}}
                    {{/*<div class="align-self-center mr-3">*/}}
                        {{/*<div class="col">*/}}
                        {{/*{{ if eq .Type 0 }}*/}}
                            {{/*assigned you*/}}
                        {{/*{{ else }}*/}}
                            {{/*gave you a task for*/}}
                        {{/*{{ end }}*/}}
                        {{/*</div>*/}}
                    {{/*</div>*/}}

                    <!-- Project Image + Task name -->
                    <div class="align-self-center row">
                        <div class="col-xs-6">
                            <img src="/{{.ProjectImage}}" alt="..." class="rounded-circle mr-2" height="42" width="42">
                        </div>
                        <div class="col-xs-6">
                        {{ if ne .Reference.Type 2}}
                        <div>
                            <a href="/{{ .ProjectID.ActorID }}/{{ .ProjectID.Project }}/{{ if eq .Reference.Type 0 }}testcases{{ else }}testsequences{{ end }}/{{ .Reference.ID }}">
                                {{ .Reference.ID }}
                            </a>
                            was assigned by
                            <a href="/users/{{ .Author }}">
                            {{ .Author }}
                            </a>
                        </div>
                        <div>
                            <span class="text-muted" style="font-size: 10pt">Version: 
                            <b>{{ .Version }}</b>
                             Variant: 
                            <b>{{ .Variant }}</b>
                            </span>
                        </div>
                        {{ else }}
                            Please assign the version "{{ .Version }}" for the variant "{{ .Variant }}" to the applicable tests in <a href="/{{ .ProjectID.ActorID }}/{{ .ProjectID.Project }}/">{{ .ProjectID.Project }}</a>
                        {{ end }}
                        </div>
                    </div>

                    <!-- Time and Done-Button -->
                    <div class="align-self-center ml-auto">
                        <span class="badge badge-secondary mr-3" data-toggle="tooltip" data-placement="top" title="{{.CreationDate}}">
                            <span>assigned </span>
                            <time class="timeago" datetime="{{ provideTimeago .CreationDate}}"></time>
                        </span>
                        <button id="btn-task-item-done-{{ .Index }}"
                                class="btn m-1 btn-sm btn-primary btn-task-item-done float-right"
                                value="{{ .Index }}"
                                type="button"
                                title="Set the task to done">Done
                        </button>
                         {{ if ne .Reference.Type 2}}
                        <button id="btn-task-item-start-{{ .Index }}"
                                class="btn m-1 btn-sm btn-success btn-task-item-start float-right"
                                value="/{{ .ProjectID.ActorID }}/{{ .ProjectID.Project }}/{{ if eq .Reference.Type 0 }}testcases{{ else }}testsequences{{ end }}/{{ .Reference.ID }}/execute?sutversion={{ .Version }}&sutvariant={{ .Variant }}"
                                type="button"
                                title="Start the execution">Start
                        </button>
                        {{ end }}
                    </div>

                </div>
            </li>

        {{else}}

            <div class="card-body text-center">
                <h4 class="card-title">Congratulations!</h4>
                <p class="card-text">Isn't an empty task-list beautiful?</p>
            </div>

        {{ end }}

    {{end}}
</ul>
<script src="/static/js/task/task-opened.js"></script>
{{end}}