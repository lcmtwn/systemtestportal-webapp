{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{define "settings-tab"}}

{{template "delete-role"}}

<div id="permission-settings-content">

    {{$editPerm := .ProjectPermissions.EditPermissions}}

    {{ if not $editPerm }}
        <div style="white-space: nowrap;">
            <h4 class="mb-3" style="color:#FFA500"><i class="fa fa-exclamation-triangle" style="color:#FFA500"></i>
                {{T "You have no permission to edit roles" .}}</h4>
        </div>
    {{ end }}

        <div id="rolesList">
        {{ $roles := .Project.Roles }}
        {{ range $index, $role := $roles }}
        {{if ne .Name "Owner"}}
            <form class="p-2">
                <h2>
                    <span class="roleName">{{.Name }}</span>
                {{if $editPerm}}
                    <button type="button" class="btn btn-danger float-right buttonDeleteRole">
                        <i class="fa fa-trash-o" aria-hidden="true"> </i>
                        <span class="d-sm-inline">{{T "Delete" .}}</span>
                    </button>
                    <button id="{{ $index }}" type="button" class="btn btn-danger d-none float-right buttonDeleteRoleConfirm">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <span class="d-none d-sm-inline">{{T "Confirm delete" .}}</span>
                    </button>
                {{end}}
                </h2>

                <div class="card-group pt-2 pb-5">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title">{{T "Execution" .}}</h6>
                            <p class="card-text">
                                <input id="{{$index}}_Execute" type="checkbox"
                                {{ if $role.Permissions.ExecutionPermissions.Execute }} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }} />
                                <label for="{{$index}}_Execute">{{T "Execute" .}}</label>
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title">{{T "Test Cases" .}}</h6>
                            <p class="card-text">
                                <input id="{{ $index }}_CreateCase" type="checkbox"
                                {{ if $role.Permissions.CasePermissions.CreateCase }} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index }}_CreateCase">{{T "Create" .}}</label><br/>

                                <input id="{{ $index }}_EditCase" type="checkbox"
                                {{ if $role.Permissions.CasePermissions.EditCase }} checked {{ end }}
                                {{ if not $editPerm }} disabled="disabled" {{ end }}/>
                                <label for="{{ $index }}_EditCase">{{T "Edit" .}}</label><br/>

                                <input id="{{ $index }}_DeleteCase" type="checkbox"
                                {{ if $role.Permissions.CasePermissions.DeleteCase}} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index }}_DeleteCase">{{T "Delete" .}}</label><br/>

                                <input id="{{ $index }}_DuplicCase" type="checkbox"
                                {{ if $role.Permissions.CasePermissions.DuplicateCase}} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index }}_DuplicCase">{{T "Duplicate" .}}</label><br/>

                                <input id="{{ $index}}_AssignCase" type="checkbox"
                                {{ if $role.Permissions.CasePermissions.AssignCase}} checked {{ end }}
                                {{if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_AssignCase">{{T "Assign" .}}</label>
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title">{{T "Test Sequences" .}}</h6>
                            <p class="card-text">
                                <input id="{{ $index}}_CreateSeq" type="checkbox"
                                {{ if $role.Permissions.SequencePermissions.CreateSequence }} checked {{ end }}
                                {{ if not $editPerm }} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_CreateSeq">{{T "Create" .}}</label><br/>

                                <input id="{{ $index}}_EditSeq" type="checkbox"
                                {{ if $role.Permissions.SequencePermissions.EditSequence}} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_EditSeq">{{T "Edit" .}}</label><br/>

                                <input id="{{ $index}}_DeleteSeq" type="checkbox"
                                {{ if $role.Permissions.SequencePermissions.DeleteSequence}} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_DeleteSeq">{{T "Delete" .}}</label><br/>

                                <input id="{{ $index}}_DuplicSeq" type="checkbox"
                                {{ if $role.Permissions.SequencePermissions.DuplicateSequence}} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_DuplicSeq">{{T "Duplicate" .}}</label><br/>

                                <input id="{{ $index}}_AssignSeq" type="checkbox"
                                {{ if $role.Permissions.SequencePermissions.AssignSequence}} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_AssignSeq">{{T "Assign" .}}</label>
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title">{{T "Members" .}}</h6>
                            <p class="card-text">
                                <input id="{{ $index}}_EditMembers" type="checkbox"
                                {{ if $role.Permissions.MemberPermissions.EditMembers }} checked {{ end }}
                                {{ if not $editPerm }} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_EditMembers">{{T "Edit" .}}</label>
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title">{{T "Settings" .}}</h6>
                            <p class="card-text">
                                <input id="{{ $index}}_EditProj" type="checkbox"
                                {{ if $role.Permissions.SettingsPermissions.EditProject}} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_EditProj">{{T "Edit" .}}</label><br/>

                                <input id="{{ $index}}_DeleteProj" type="checkbox"
                                {{ if $role.Permissions.SettingsPermissions.DeleteProject}} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_DeleteProj">{{T "Delete" .}}</label><br/>

                                <input id="{{ $index}}_EditPerm" type="checkbox"
                                {{ if $role.Permissions.SettingsPermissions.EditPermissions}} checked {{ end }}
                                {{ if not $editPerm}} disabled="disabled" {{ end }}/>
                                <label for="{{ $index}}_EditPerm">{{T "Edit Perm." .}}</label>
                            </p>
                        </div>
                    </div>
                </div>
            </form>
        {{ end }}
        {{ end }}
        </div>

</div>

<!-- Import Scripts here -->
<script src="/static/js/project/settings/settings-permissions.js"></script>

<script>

    // Saves all current roles --> variable only used in /static/js/project/settings/settings-permission.js
    var globalVarOldRoles = [];
    {{ range $index, $role := $roles }}
        {{ if ne $index "Owner"}}
            globalVarOldRoles.push({{$index}});
        {{ end }}
    {{ end }}

    initializePermissionSettingsListener()
</script>
{{end}}