{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{define "tab-content"}}
{{template "modal-generic-error" .}}

<div class="tab-card card" id="tabTestDashboard">

    <!-- Nav Bar -->
    <nav class="navbar navbar-light action-bar p-3">
        <div class="input-group flex-nowrap">

            <label for="versions-dropdown-menu" class="control-label pt-2 pr-2">
                <strong>Variant</strong>
            </label>


            <select class="selectpicker custom-select pt-2 col-4 col-lg-2 rounded" data-style="btn-primary" id="versions-dropdown-menu" name="versions-dropdown-menu" title="">
                {{/*get filled by fillDropdown*/}}
            </select>

            <span class="ml-auto">
                <input id="dashboardToggle" checked data-toggle="toggle" data-on="Test Cases" data-off="Test Sequences" data-offstyle="primary" type="checkbox" title="">
            </span>

        </div>
    </nav>

    <!-- Content -->
    <div class="row tab-side-bar-row">
        <div class="col-md-12 p-3">
            <h4 class="mb-3">
                {{T "Dashboard" .}}
            </h4>
            <div class="panel panel-default table-responsive">
                <table class="table table-bordered" id="dashboardTable">
                    <thead>
                    <tr id="variantNames">
                        <th>
                            {{T "Test Cases" .}}
                        </th>
                    </tr>
                    </thead>
                    <tbody id="dashboard-content">
                    {{/*get filled by onChangeDropdown and newSiteDashboard*/}}
                    <td colspan="5" class="text-center">... {{T "Loading" .}} ...</td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/dashboard.js"></script>

<script>

    //Define strings for JavaScript here
    var stringTestCases = "" + {{T "Test Cases" .}};
    var stringTestSequences = "" + {{T "Test Sequences" .}};
    var stringTestSequenceNotYetExecuted = "" + {{T "Test sequence not yet executed" .}};
    var stringNotAssessed = "" + {{T "Not assessed"}};
    var stringPassed = "" + {{T "Passed" .}};
    var stringPartiallySuccessful = "" + {{T "Partially Successful" .}};
    var stringFailed = "" + {{T "Failed" .}};
    var stringTestSequenceIsNotApplicable = "" + {{T "Test sequence is not applicable for this version" .}};
    var stringTestCaseNotYetExecuted = "" + {{T "Test case not yet executed" .}};
    var stringTestCaseIsNotApplicable = "" + {{T "Test case is not applicable for this version" .}};
    var stringResult = "" + {{T "Result" .}};
    var stringTester = "" + {{T "Tester" .}};
    var stringComment = "" + {{T "Comment" .}};
    var stringDate = "" + {{T "Date" .}};
    var stringNoComment = "" + {{T "No comment" .}};

    $("#printerIcon").addClass("d-none");
    $("#helpIcon").addClass("d-none");
    var casesDashboardElements = {{.Dashboard.DashboardElements}};
    var sequencesDashboardElements = {{.DashboardSequences.DashboardElements}};
    var versions = {{.Dashboard.Versions}};
    var variants = {{.Dashboard.Variants}};

    fillDropdown(variants);

    if ($(dashboardToggle).prop('checked')) {
        newSiteDashboard(casesDashboardElements, versions, variants, true);
    } else {
        newSiteDashboard(sequencesDashboardElements, versions, variants, false);
    }

    // On document ready
    $(() => {
        $(dashboardToggle).bootstrapToggle({
            on: stringTestCases,
            off: stringTestSequences,
            offstyle: "primary"
        });
    });
</script>
{{end}}