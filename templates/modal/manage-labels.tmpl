{{define "modal-manage-labels"}}
{{$maxNameLength:=2000}}

{{template "delete-pop-up" .DeleteLabels}}
<!-- User error messages-->
<div class="modal fade" style="z-index: 9999" id="errorMessageDialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Error</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="errorMessage">
                <p></p>
            </div>
            <div class="modal-footer">
                <button id="buttonClose" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- unsaved changes when switching labels dialog -->
<div class="modal fade" style="z-index: 9999" id="unsaved-Changes-Switching-Dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Switching labels with unsaved changes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Some changes are not saved yet. Continue?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonApplyChangesSwitching">Apply Changes</button>
                <button type="button" class="btn btn-danger" id="buttonDiscardChangesSwitching">Discard Changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Abort</button>
            </div>
        </div>
    </div>
</div>

<!-- unsaved changes when closing dialog -->
<div class="modal fade" style="z-index: 9999" id="unsaved-Changes-Closing-Dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Exiting with unsaved changes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Some changes are not saved yet. Continue?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonApplyChangesClosing">Apply Changes</button>
                <button type="button" class="btn btn-danger" id="buttonDiscardChangesClosing">Discard Changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Abort</button>
            </div>
        </div>
    </div>
</div>

<!-- Label-Modal -->
<div class="modal fade" id="modal-manage-labels" tabindex="-1" role="dialog" aria-labelledby="modal-manage-labels-label"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 750px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-manage-labels-label"><i class="fa fa-wrench" aria-hidden="true"></i>
                    Manage Labels</h5>
                <button type="button" class="close" onclick="closeManageLabels()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div>
                <div class="modal-body"
                     style="width: 30%; float:left; padding-bottom: 0px;">
                    <button id="buttonNewLabelProjectLabel" type="button" class="btn label-list-btn-selected"
                            style="opacity: 1; font-size: 1.25rem; margin-bottom: 5px;">New Label
                    </button>
                    <h5>Labels</h5>
                    <div class="row" style="overflow-y: auto; max-height: 212px;">
                        <div class="col-12">
                            <p id="projectLabelList">
                            </p>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="modal-body" style="width:70%; float:left;  border-left: 1px solid #e9ecef">
                    <h5 id="modeName">New Label</h5>
                    <div class="row">
                        <div class="col-12">
                            <form id="form-label-inputs">
                                <div class="input-group mb-3">
                                    <span class="input-group-addon" id="label-name-label">Name</span>
                                    <input type="text" class="form-control" id="inputProjectLabelName"
                                           pattern="[\sA-Za-z0-9-_().,:äöüÄÖÜß]{1,{{$maxNameLength}}}" required
                                           placeholder="e.g. GUI"
                                           aria-label="e.g. GUI" aria-describedby="label-name-label"
                                           oninvalid="setCustomValidity('Please use 1-{{$maxNameLength}} letters, numbers or .,:_() .')"
                                           oninput="setCustomValidity('')">
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-addon" id="label-description-label">Description</span>
                                    <input type="text" class="form-control" id="inputProjectLabelDescription"
                                           placeholder="e.g. Tests for the Graphical User Interface"
                                           aria-label="e.g. Tests for the Graphical User Interface"
                                           aria-describedby="label-description-label">
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-addon" id="label-color-label">Color</span>
                                    <div class="color-selector-box form-control" id="colorSelectorBoxes"
                                         style="overflow-y: auto">
                                    {{range .LabelColors}}
                                        <span class="color-selector clickIcon" id="{{ . }}"
                                              style="background-color: #{{ . }};"
                                              onclick="highlight(this)"></span>
                                    {{end}}
                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-addon" id="label-preview-label">Preview</span>
                                    <div class="color-selector-box form-control">
                                        <span id="preview-label" class="badge badge-primary"
                                              style="max-width: 100%; white-space: normal;
                                              background-color: #007bff;">Preview</span>
                                    </div>
                                </div>
                                <button id="buttonSaveProjectLabel" class="btn btn-primary float-right" type="button">
                                    Save
                                </button>
                                <button id="buttonDeleteProjectLabel" class="btn btn-danger float-left" type="button"
                                        disabled data-toggle="modal" data-target="#deleteLabels">Delete
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="closeButtonBottom" type="button" class="btn btn-secondary" onclick="closeManageLabels()">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/util/common.js"></script>

<script type='text/javascript'>

    var xmlhttp = new XMLHttpRequest();
    // projectLabelData contains the labels of a project
    var projectLabelData = {};
    var labelIdCounter = 0;
    var urlLabels = getProjectURL().appendSegment("labels").toString();

    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            projectLabelData = JSON.parse(this.responseText);
            if (projectLabelData === null) {
                projectLabelData = [];
            }
            fillLabels(projectLabelData, "#projectLabelList");
            newLabel();
        }
    };
    xmlhttp.open("GET", urlLabels, true);
    xmlhttp.send();

    // fillLabels gets a map with labels and adds them to the list with the selectorID
    function fillLabels(labelMap, selectorID) {
        $(selectorID).empty();
        $.each(labelMap, function (key, label) {
            addLabel(label, selectorID);
        });
    }

    // addLabel adds a label to the list
    function addLabel(label, selectorID) {
        if (label.Color === "") {
            label.Color = "007bff";
        }
        var selector = $(selectorID);

        selector.append($("<span></span>")
                .attr("id", "label-" + labelIdCounter)
                .attr("type", "button")
                .attr("class", "btn label-list-btn")
                .attr("data-toggle", "tooltip")
                .attr("data-placement", "left")
                .attr("data-original-title", label.Description)
                .attr("onclick", "labelSelected('" + "label-" + labelIdCounter + "' ,'" + specialCharacterName(label.Name) + "'," +
                        " '" + label.Description + "', '" + label.Color + "')")
                .attr("style", "background-color:#" + label.Color + ";"
                        + "border-color:#" + label.Color + ";" + " white-space: normal;")
                .text(label.Name));

        labelIdCounter = labelIdCounter + 1;
    }

    //disables the description when an error message is shown
    $("#errorMessageDialog").on("shown.bs.modal", function () {
        $("#inputProjectLabelDescription").attr("disabled", true);
        $("#inputProjectLabelDescription").blur();
    });

    //enables the description when an error message is hidden
    $("#errorMessageDialog").on("hidden.bs.modal", function () {
        $("#inputProjectLabelDescription").attr("disabled", false);
        $("#inputProjectLabelDescription").focus();
    });

    /* Handler for enter-key*/
    $("#inputProjectLabelName").keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            $("#inputProjectLabelDescription").focus();
        }
    });

    //updates the label in the GUI and the preview after an input is made
    $("#inputProjectLabelName").keyup(function (event) {
        updatePreview("name", $("#inputProjectLabelName").val().trim());
        if ($("#inputProjectLabelName").val().trim() == "") {
            updatePreview("name", "Preview");
        }
        //updates the label name in the label lists
        if (selectedLabelName != "") {
            if ($("#inputProjectLabelName").val().trim() == "") {
                $("#" + selectedLabelId).html("Preview");
            } else {
                $("#" + selectedLabelId).html($("#inputProjectLabelName").val().trim());
            }
        }
    });

    //updates the label description for the label list
    $("#inputProjectLabelDescription").keyup(function () {
        if (selectedLabelName != "") {
            $("#" + selectedLabelId).attr("data-original-title",
                    $("#inputProjectLabelDescription").val().trim());
        }
    });

    //Handler for the enter key at the description
    $("#inputProjectLabelDescription").keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();

            //save the label
            saveProjectLabel();
        }
    });

    //updates the preview color and label GUI after the color changed
    {{range .LabelColors}}
    $("#{{ . }}").click(function (event) {
        updatePreview("color", "{{ . }}")
        if (selectedLabelName != "") {
            $("#" + selectedLabelId).attr("style", "background-color:#" + "{{ . }}"
                    + ";" + "border-color:#" + "{{ . }}" + ";");
        }
    });
    {{end}}

    //allows the Usage of special characters
    function specialCharacterName(name) {
        var newName = name.replace(/[!"#$%&'()*+,./:;<=>?@[\\\]^`{|}~]/g, "\\$&");
        return newName;
    }

    //allows the usage of "
    function specialCharacterJSON(text) {
        var newText = text.replace(/["\\]/g, "\\$&");
        return newText;
    }

    //updates the badge preview
    function updatePreview(attr, val) {
        if (attr == "name") {
            if (val == "") {
                val = $("#inputProjectLabelName").val().trim();
            }
            $("#preview-label").html(val);
        } else if (attr == "color") {
            $("#preview-label").attr("style", "max-width: 100%; " +
                    "background-color: #" + val + ";");
        }
    }

    /* clears the text fields and resets the label preview*/
    function clearFields() {
        resetLabelGUIChanges();
        // Clear text field
        $("#inputProjectLabelName").val("");
        $("#inputProjectLabelDescription").val("");

        //Reset Preview Label and color highlighting
        updatePreview("name", "Preview");
        updatePreview("color", "007bff");
        highlight(document.getElementById(defaultColor));
    }

    // resets GUI changes of the selected label
    function resetLabelGUIChanges() {
        if (selectedLabelName != "") {
            $("#" + selectedLabelId).attr("data-original-title", selectedLabelDescription);
            $("#" + selectedLabelId).attr("style", "background-color:#" + selectedLabelColor
                    + ";" + "border-color:#" + selectedLabelColor + ";");
            $("#" + selectedLabelId).html(selectedLabelName);
        }
    }

    // sets the user-error-message and opens the collapse window
    function setUserErrorMessage(message) {
        $("#errorMessage").text(message);
        $("#errorMessageDialog").modal("show");
    }

    //used to save the data of the currently selected label
    var selectedLabelId = "";
    var selectedLabelName = "";
    var selectedLabelColor = "007bff";
    var selectedLabelDescription = "";

    //used by labelSelected() to temporarily save the data of the next selected label if there are unsaved changes
    var nextSelectedLabelId = "";
    var nextSelectedLabelName = "";
    var nextSelectedLabelColor = "007bff";
    var nextSelectedLabelDescription = "";

    /* attach a handler to the Save button */
    $("#buttonSaveProjectLabel").click(function (event) {
        saveProjectLabel();
    });

    //decides whether a label gets added or edited.
    //labels are added if the window is in the add dialog
    //labels are edited if the window is in the edit dialog
    //returns true if there was no userError Message
    function saveProjectLabel() {

        // Check label input using defined pattern
        const form = $("#form-label-inputs")[0];
        if (!form.checkValidity()) {
            form.reportValidity();
            return false
        }

        var labelName = $("#inputProjectLabelName").val().trim();
        var labelDesc = $("#inputProjectLabelDescription").val().trim();
        var labelColor = $("#preview-label").attr("style");
        labelColor = labelColor.substring(36, 42);

        if ((labelName !== "") && (labelName.length <= {{$maxNameLength}}) && selectedLabelChanged()) {
            //editing a label
            if (selectedLabelName != "" && (!containsLabel(projectLabelData, labelName) || labelName === selectedLabelName)) {
                editAndSaveLabel(labelName, labelDesc, labelColor);
                return true;
            }
            //adding a new label
            else if (selectedLabelName == "" && (!containsLabel(projectLabelData, labelName))) {
                addAndSaveLabel(labelName, labelDesc, labelColor);
                return true;
            }
            //label already taken
            else {
                setUserErrorMessage("Desired label name is already in use.");
                return false;
            }
        }
    }

    //adds a label and saves it. The window is in the editing dialog afterwards
    function addAndSaveLabel(labelName, labelDesc, labelColor) {
        projectLabelData.push(JSON.parse('{"Name":"' + specialCharacterJSON(labelName) + '","Description":"' + specialCharacterJSON(labelDesc) + '","Color":"' + labelColor + '"}'));

        //should be called before addLabel
        changeCurrentlySelectedLabel("label-" + labelIdCounter, labelName, labelDesc, labelColor);

        addLabel({Name: labelName, Description: labelDesc, Color: labelColor}, "#projectLabelList");
        $("#modeName").text("Edit Label");
        $("#buttonSaveProjectLabel").text("Save");
        highlightLabelBtn(selectedLabelId);
        sortProjectLabelData();
        saveLabel();
        $("#buttonNewLabelProjectLabel").attr("class", "btn label-list-btn");
        $("#buttonDeleteProjectLabel").prop("disabled", false);

        var tips = $('[data-toggle="tooltip"]');
        if (tips != null) {
            tips.tooltip({
                trigger: 'hover'
            });
        }
    }

    //edits a label and saves it
    function editAndSaveLabel(labelName, labelDesc, labelColor) {
        let projectLabelUpdateData = [];
        projectLabelUpdateData.push(JSON.parse('{"Name":"' + specialCharacterJSON(selectedLabelName) + '","Description":"' + specialCharacterJSON(selectedLabelDescription) + '","Color":"' + selectedLabelColor + '"}'));
        projectLabelUpdateData.push(JSON.parse('{"Name":"' + specialCharacterJSON(labelName) + '","Description":"' + specialCharacterJSON(labelDesc) + '","Color":"' + labelColor + '"}'));
        saveEditLabel(projectLabelUpdateData, labelName, labelDesc, labelColor, selectedLabelName);
        editLabelButton(selectedLabelId, labelName, labelDesc, labelColor);
        deleteLabel(selectedLabelName, projectLabelData);
        projectLabelData.push(JSON.parse('{"Name":"' + specialCharacterJSON(labelName) + '","Description":"' + specialCharacterJSON(labelDesc) + '","Color":"' + labelColor + '"}'));
        sortProjectLabelData();
        changeCurrentlySelectedLabel(selectedLabelId, labelName, labelDesc, labelColor);

        var tips = $('[data-toggle="tooltip"]');
        if (tips != null) {
            tips.tooltip({
                trigger: 'hover'
            });
        }
    }

    //sorts projectLabelData by names
    function sortProjectLabelData() {
        projectLabelData.sort(function (label1, label2) {
            if (label1.Name < label2.Name) {
                return -1;
            } else if (label1.Name > label2.Name) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    /* attach a handler to the New Label button */
    $("#buttonNewLabelProjectLabel").click(function (event) {
        newLabel();
    });

    //changes the currently selected label
    function changeCurrentlySelectedLabel(labelId, labelName, labelDescription, labelColor) {
        selectedLabelId = labelId;
        selectedLabelName = labelName;
        selectedLabelDescription = labelDescription;
        selectedLabelColor = labelColor;
    }

    //resets the GUI to the new label dialog
    function newLabel() {
        resetLabelGUIChanges();
        $("#modeName").text("Add Label");
        changeCurrentlySelectedLabel("", "", "", defaultColor);
        clearFields();
        highlightLabelBtn("");
        $("#buttonSaveProjectLabel").text("Add");
        $("#buttonNewLabelProjectLabel").attr("class", "btn label-list-btn-selected");
        $("#buttonDeleteProjectLabel").prop("disabled", true);
    }

    //returns true if there are unsaved changes
    function selectedLabelChanged() {
        var currentLabelName = $("#inputProjectLabelName").val().trim();
        var currentLabelDescription = $("#inputProjectLabelDescription").val().trim();
        var currentLabelColor = pickedColorspan.getAttribute("id");

        //no changes
        if (selectedLabelName === currentLabelName
                && selectedLabelDescription === currentLabelDescription
                && selectedLabelColor === currentLabelColor) {
            return false;
        }
        //changes
        else {
            return true;
        }
    }

    //discards all changes, hides the unsaved changes dialog and selects the next label
    $("#buttonDiscardChangesSwitching").click(function () {
        $("#inputProjectLabelName").val(selectedLabelName);
        $("#inputProjectLabelDescription").val(selectedLabelDescription);
        $("#colorSelectorBoxes").children().each(function () {
            if (this.getAttribute("id") === selectedLabelColor) {
                highlight(this);
            } else {
                this.setAttribute("class", "color-selector clickIcon");
            }
        });
        resetLabelGUIChanges();
        $("#unsaved-Changes-Switching-Dialog").modal("hide");
        labelSelected(nextSelectedLabelId, nextSelectedLabelName, nextSelectedLabelDescription, nextSelectedLabelColor);
    });

    //saves all changes and selects the next label
    $("#buttonApplyChangesSwitching").click(function () {
        if (saveProjectLabel()) {
            $("#unsaved-Changes-Switching-Dialog").modal("hide");
            labelSelected(nextSelectedLabelId, nextSelectedLabelName, nextSelectedLabelDescription, nextSelectedLabelColor);
        } else {
            $("#unsaved-Changes-Switching-Dialog").modal("hide");
        }
    });

    //opens a dialog if there are unsaved changes when closing the window
    function closeManageLabels() {
        if (selectedLabelChanged()) {
            $("#unsaved-Changes-Closing-Dialog").modal("show");
        } else {
            newLabel();
            $("#modal-manage-labels").modal("hide");
        }
    }

    //discards changes and closes both dialogs
    $("#buttonDiscardChangesClosing").click(function () {
        newLabel();
        $("#modal-manage-labels").modal("hide");
        $("#unsaved-Changes-Closing-Dialog").modal("hide");
    });

    //saves the changed and closes both dialogs
    $("#buttonApplyChangesClosing").click(function () {
        if (saveProjectLabel()) {
            newLabel();
            $("#modal-manage-labels").modal("hide");
            $("#unsaved-Changes-Closing-Dialog").modal("hide");
        } else {
            $("#unsaved-Changes-Closing-Dialog").modal("hide");
        }
    });

    /*opens a dialog if there are unsaved changes.
    / fills the input fields with the data of the selected label.
    / highlights the color of the selected label
    / temporarily saves the data of the currently selected label*/
    function labelSelected(labelId, labelName, labelDescription, labelColor) {
        //notifies the user of unsaved changes when switching the selected Label
        if (selectedLabelChanged()) {
            nextSelectedLabelId = labelId;
            nextSelectedLabelName = labelName;
            nextSelectedLabelDescription = labelDescription;
            nextSelectedLabelColor = labelColor;
            $("#unsaved-Changes-Switching-Dialog").modal("show");
        } else {
            $("#modeName").text("Edit Label");
            $("#buttonSaveProjectLabel").text("Save");

            //fill input fields
            $("#inputProjectLabelName").val(labelName);
            $("#inputProjectLabelDescription").val(labelDescription);
            updatePreview("name", labelName);
            updatePreview("color", labelColor);

            changeCurrentlySelectedLabel(labelId, labelName, labelDescription, labelColor);

            $("#buttonDeleteProjectLabel").attr("disabled", false);

            highlightLabelBtn(selectedLabelId);

            $("#buttonNewLabelProjectLabel").attr("class", "btn label-list-btn");

            //highlights the color checkbox
            $("#colorSelectorBoxes").children().each(function () {
                if (this.getAttribute("id") === labelColor) {
                    highlight(this);
                } else {
                    this.setAttribute("class", "color-selector clickIcon");
                }
            });
        }
    }

    //highlights the currently selected Label button
    function highlightLabelBtn(labelId) {
        $("#projectLabelList").children().each(function () {
            if (this.getAttribute("id") === labelId) {
                this.setAttribute("class", "btn label-list-btn-selected");
            } else {
                this.setAttribute("class", "btn label-list-btn");
            }
        });
    }

    // containsLabel checks whether the list of project labels
    // already contains a label with the labelName
    // Returns true if the list contains a label, else return false
    function containsLabel(projectLabels, labelName) {
        var contains = false;
        projectLabels.forEach(function (projectLabel) {
            if (projectLabel.Name === labelName) {
                contains = true;
            }
        });
        return contains;
    }

    //deletes the currently selected label
    $("#buttonDeleteLabels").click(function (event) {
        $(".tooltip").hide();
        $('#deleteLabels').modal('hide');
        removeLabel(selectedLabelId, selectedLabelName);
        newLabel();
        saveLabel();
    });

    //edits the label button in the GUI
    function editLabelButton(labelId, newName, description, color) {
        $("#" + labelId).attr("data-original-title", description);
        $("#" + labelId).attr("onclick", "labelSelected('" + labelId + "', '" + specialCharacterName(newName) + "', '" + description + "', '" + color + "')");
        $("#" + labelId).attr("style", "background-color:#" + color + ";" + "border-color:#" + color + ";");
        $("#" + labelId).html(newName);
    }

    // removeLabel removes a label from the GUI
    function removeLabel(labelId, name) {
        var elem = document.getElementById(labelId);
        elem.parentNode.removeChild(elem);
        deleteLabel(name, projectLabelData);
        return false;
    }

    // deleteLabel deletes a label with the given name from the given list.
    function deleteLabel(name, list) {
        list.forEach(function (value, index) {
            if (value.Name === name) {
                list.splice(index, 1)
            }
        });
        return list;
    }

    /* saves the Labels and label changes*/
    function saveLabel() {
        var urlLabelsPost = getProjectURL().appendSegment("labels").toString();

        var xhr = new XMLHttpRequest();
        xhr.open("POST", urlLabelsPost, true);
        xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

        // send the collected data as JSON
        xhr.send(JSON.stringify(projectLabelData));


        xhr.onloadend = function () {
            if (this.status === 200) {
                // Update labels in gui
                showFilterLabels(projectLabelData, "#labelContainer", getProjectTabURL().toString());

            } else {
                console.log(this.response);
            }
        };

    }

    /* edits a label*/
    function saveEditLabel(projectLabelUpdateData, labelName, labelDesc, labelColor, oldName) {
        var urlLabelsPut = getProjectURL().appendSegment("labels").toString();

        var xhr = new XMLHttpRequest();
        xhr.open("PUT", urlLabelsPut, true);
        xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        // send the collected data as JSON
        xhr.send(JSON.stringify(projectLabelUpdateData));


        xhr.onloadend = function () {
            if (this.status === 200) {
                // Update labels in gui
                showFilterLabels(projectLabelData, "#labelContainer", getProjectTabURL().toString());
                updateLabelInTestCaseAndTestSequenceList(labelName, labelDesc, labelColor, oldName);
            } else {
                console.log(this.response);
            }
        };

    }

    function updateLabelInTestCaseAndTestSequenceList(labelName, labelDesc, labelColor, oldName) {
        $(".sorting_1").children().each(function () {
            if (this.textContent.trim() === oldName) {
                this.textContent = labelName;
                this.setAttribute("id", labelName);
                this.setAttribute("data-original-title", labelDesc);
                this.setAttribute("style", "background-color: " + "#" + labelColor + ";");
            }
        });
    }

    var defaultColor = "007bff";

    var pickedColorspan = document.getElementById(defaultColor);

    pickedColorspan.setAttribute("class", "color-selector-picked clickIcon")

    // highlights a color selector
    function highlight(colorspan) {
        var oldPickedColorspan = pickedColorspan;
        pickedColorspan = colorspan;
        oldPickedColorspan.setAttribute("class", "color-selector clickIcon");
        pickedColorspan.setAttribute("class", "color-selector-picked clickIcon");

    }
</script>
{{end}}